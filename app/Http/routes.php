<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('wilayah', 'WilayahController@showWilayah');
Route::get('admin', 'AdminController@index');
Route::get('draw', 'DrawAreaController@showPage');
Route::get('search', 'SearchController@getSearchData');
Route::get('search/inner/{qSearch}', 'SearchController@_searchInner');
//Route::get('login', 'HomeController@login');
Route::get('dashboard', 'HomeController@dashboard');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('form-wisata', 'FormController@formWisata');
Route::get('form-inap', 'FormController@formInap');
Route::get('form-oleh', 'FormController@formOleh');
Route::get('form-transit', 'FormController@formTransit');
Route::get('form-wilayah', 'FormController@formWilayah');
Route::get('admin', 'FormController@admin');
Route::get('dummy-transit', 'TransitController@searchAllAirports');
Route::get('profile', 'WilayahController@editProfile');
Route::get('user-plan', 'PlannerController@showUserPlan');
Route::get('show', 'HomeController@show');

Route::group(['prefix' => 'wilayah'], function () {
    Route::get('{id}', 'WilayahController@show');

});

Route::group(['prefix' => 'planner'], function () {
    Route::get('tempat/add/{id}', 'PlannerController@addTempatToPlaner');
    Route::get('{id}', 'PlannerController@show');
    Route::post('analysis', 'PlannerController@planAnalysis');
    Route::post('save', 'PlannerController@saveToDb');
    Route::post('delete', 'PlannerController@deletePlan');

});

Route::group(['prefix' => 'wisata'], function () {
    Route::get('/', 'TempatController@show');
});
Route::get('main/search/{qSearch}', 'TempatController@_search');

Route::get('sidebar/wisata/load/{id}', 'TempatController@_loadTempatWisataByWilayah');
Route::get('sidebar/penginapan/load/{id}', 'TempatController@_loadPenginapanByWilayah');
Route::get('sidebar/oleh/load/{id}', 'TempatController@_loadTempatOlehOlehByWilayah');
Route::get('sidebar/transit/load', 'TempatController@_loadTransitByWilayah');

Route::get('transportasi/pesawat/{departure}/{arrival}/{date}/{adult}/{child}/{infant}', 'TransportasiController@searchFlights');
Route::get('transportasi/kereta/{departure}/{arrival}/{date}/{adult}/{child}', 'TransportasiController@searchTrains');
Route::get('detail/wisata/{flag}/{id}', 'TempatController@_loadDetailTempat');
Route::get('tempat/transitByCode/{kode}', 'TempatController@getTransitByCodeTransit');

