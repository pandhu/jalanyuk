<?php
namespace App\Http\Controllers;

/**
* Ini class untuk menampilkan halaman wilayah
*/
use App\Models\Penginapan;
use App\Models\Tempat;
use App\Models\TempatOlehOleh;
use App\Models\TempatWisata;
use App\Models\Wilayah;
use App\Models\Planner;
use App\Models\PlannerWisata;
use Ivory\GoogleMap\Map;
use Ivory\GoogleMap\Helper\MapHelper;
class WilayahController extends BaseController
{
	public function showWilayah()
	{
        $map = new Map();
        $map->setStylesheetOption('width', '100%');
        $map->setStylesheetOption('z-index', '0');
        $map->setAutoZoom(false);
        $map->setCenter(-7.771447,110.377423, true);

        $mapHelper = new MapHelper();
        return view('wilayah')->with(array(
            'map'=>$map,
            'mapHelper'=>$mapHelper
        ));
	}

    public function show($id){
        $wilayah = Wilayah::find($id);
        $tempat = Tempat::get();
        //$wisataOnPlanner = Planner::find(1)->wisata;
        //$planner_wisata =PlannerWisata::where('id_planner', 1)->get();
        $center['lat'] = $wilayah->lat;
        $center['lng'] = $wilayah->lng;
        return view('wilayah', ['center'=>$center,
            'zoom'=>10,
            'wilayah'=>$wilayah,
            'tempat'=> $tempat,
            'tempat_json' => null,
            //'planner_wisata' => $planner_wisata,
          //  'wisataOnPlanner' => $wisataOnPlanner
            ]);

    }

    public function editProfile(){
        return view('profile');
    }

    public function showPlans(){
        return view('user-plan');
    }
}