<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TransitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    public $secretKey = 'a6743f1cb3ff9a545701be82f34ebc88';
    
    public function searchAllAirports()
    {
        $token = $this->getToken($this->secretKey);
        $airport = $this->getAllAirports($token);
        return view('dummy-transit', ['airports' => $airport]);
    }

    public function searchAllStations()
    {
        $token = $this->getToken($this->secretKey);
        $station = $this->getAllStations($token);
        return view('dummy-transit', ['stations' => $station]);
    }

    //Mengambil token dari API tiket.com
    public function getToken($secretKey)
    {
        $url = 'https://api-sandbox.tiket.com/apiv1/payexpress?method=getToken&secretkey='.$secretKey.'&output=json';
        $jsonToken = file_get_contents($url);
        $arrToken = json_decode($jsonToken);
        return $arrToken->token;
    }

    //Mengambil array semua Airport dari API tiket.com
    public function getAllAirports($token)
    {
        $url = 'https://api-sandbox.tiket.com/flight_api/all_airport?token='.$token.'&output=json';
        $jsonAirport = file_get_contents($url);
        $arrAirport = json_decode($jsonAirport);
        return $arrAirport;
    }

    //Mengambil array semua Station dari API tiket.com
    public function getAllStations($token)
    {
        $url = 'https://api-sandbox.tiket.com/train_api/train_station?token='.$token.'&output=json';
        $jsonStation = file_get_contents($url);
        $arrStation = json_decode($jsonStation);
        return $arrStation;
    }
}
