<?php

namespace App\Http\Controllers;

use App\Models\Tempat;
use App\Models\Gallery;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Wilayah;
class TempatController extends Controller
{

    

    public function _search($qSearch){
        $result = Tempat::where('nama', 'like', '%'.$qSearch.'%')->get();
        return view('_inner_search', ['result'=>$result]);
    }

    public function _loadTempatWisataByWilayah($id){
        $tempat = Tempat::where('flag', 2)->where('id_wilayah', $id)->get() ;
        return view('partials._tempat', array('tempat'=>$tempat));

    }

    public function _loadPenginapanByWilayah($id){
        $tempat = Tempat::where('flag', 0)->where('id_wilayah', $id)->get() ;
        return view('partials._tempat', array('tempat'=>$tempat));

    }

    public function _loadTempatOlehOlehByWilayah($id){
        $tempat = Tempat::where('flag', 1)->where('id_wilayah', $id)->get() ;
        return view('partials._tempat', array('tempat'=>$tempat));

    }

    public function _loadTransitByWilayah(){
        $bandara = Tempat::where('flag', 3)->where('jenis', 'Bandara')->get() ;
        $stasiun = Tempat::where('flag', 3)->where('jenis', 'Stasiun')->get() ;
        return view('partials._transit', array('bandara'=>$bandara, 'stasiun'=>$stasiun));
    }

    public function _loadDetailTempat($flag, $id){
        $tempat = Tempat::where('flag', $flag)->where('id', $id)->first();
        $gallery = Gallery::where('id_tempat', $tempat->id)->get();
        
        if ($flag == 0){
            return view('partials._detailPenginapan', array('tempat'=>$tempat, 'gallery' => $gallery));
        } elseif ($flag == 1) {
            return view('partials._detailOleh', array('tempat'=>$tempat, 'gallery' => $gallery));
        } else {
            return view('partials._detailWisata', array('tempat'=>$tempat, 'gallery' => $gallery));
        } 
    }

    public function getTransitByCodeTransit($kode){
        $transit = Tempat::where('kode', $kode)->first();
        return $transit;
    }
}
