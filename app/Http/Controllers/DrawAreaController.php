<?php
namespace App\Http\Controllers;

/**
* Ini class untuk menampilkan halaman wilayah
*/

class DrawAreaController extends BaseController
{
	public function showPage()
	{
        return view('drawarea');
	}
}