<?php

namespace App\Http\Controllers;

use App\Models\Planner;
use App\Models\PlannerTempat;
use App\Models\Tempat;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PlannerController extends Controller
{
   public function addTempatToPlaner($id){
       $tempat = Tempat::find($id);
       $foto = $tempat->gallery->first();
       if($foto == null){
        $foto = 'images/ap.jpg';
       } else {
        $foto = $foto->foto;
       }
       $tempat->image_url = $foto;
       $json = json_encode($tempat);
       return view('partials._plannerItem', array('tempat'=>$tempat, 'json'=>json_encode($tempat)));
   }

    public function saveToDb(Request $request){
        $data = Input::all();
        $nama = $data['name'];
        $planner_arr = json_decode($data['planner']);

        $planner = new Planner();

        if(Auth::check())
            $planner->id_user = Auth::user()->username;

        $planner->nama = $nama;
        $planner->save();
        $position = 0;
        foreach ($planner_arr as $item){
            //$item = json_decode($item);
            //var_dump($item);
            //die();
            $planner_tempat = new PlannerTempat();
            $planner_tempat->id_planner = $planner->id;
            $planner_tempat->id_tempat = $item->id;
            $planner_tempat->no_urut = $position;
            $planner_tempat->save();

            $position++;
        }

    }

    public function deletePlan(Request $request){
        $message ="1";
        
        if(Auth::check()){

            $data = Input::all();
            $id_planner = $data['id_planner'];
            $planner = PLanner::find($id_planner)->first();
            $auth_username = Auth::user()->username;
            $planner_username = $planner->id_user;
            if($auth_username == $planner_username)
            {
                $planner_tempat = PlannerTempat::where('id_planner',$planner->id)->get();
                foreach ($planner_tempat as $item) {
                    //$item->delete();
                    //DB::table('planner_tempat')->where('id_planner',$item->id_planner)
                    PlannerTempat::where('id_planner',$item->id_planner)->where('id_tempat',$item->id_tempat)->delete();
                }
                //$planner->delete();
                Planner::where('id',$planner->id)->delete();
                //dd("asd");
                $message = "delete success";
            }            
        }

        //$this->showUserPlan();
    }

    public function show($id){
        $planner = Planner::find($id);
        $tempat = $planner->tempat()->get();
        $wilayah = $tempat[0]->wilayah()->first();
        $center['lat'] = $wilayah->lat;
        $center['lng'] = $wilayah->lng;
        $tempat_json = json_encode($tempat);
        //dd($tempat_json);
        return view('wilayah', ['center'=>$center,
            'zoom'=>10,
            'wilayah'=>$wilayah,
            'tempat'=> $tempat,
            'tempat_json' => $tempat_json
            //'planner_wisata' => $planner_wisata,
            //  'wisataOnPlanner' => $wisataOnPlanner
        ]);

    }

    public function showUserPlan(){
        $plans = Planner::where('id_user', Auth::user()->username)->get();
        $divider = count($plans)/2;
        //dd($plans);
        if(count($plans) == 1)
            $divider =1;
        return view('user-plan', array('plans'=>$plans, 'divider'=>$divider));
    }

    public function planAnalysis(Request $request){
        $data = Input::all();
        $instructions_arr =json_encode($data['instructions']);
        // $instructions = array();
        // foreach ($instructions_arr as $item){
        //     array_push($instructions, $item);
        // }

        return view('partials._planAnalisis', array('instructions'=>$instructions_arr));
    }
}
