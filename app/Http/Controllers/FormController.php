<?php
namespace App\Http\Controllers;

class FormController extends BaseController {
	public function formWisata(){
		return view('form-wisata');
	}

	public function formInap(){
		return view('form-inap');
	}

	public function formOleh(){
		return view('form-oleh');
	}

	public function formTransit(){
		return view('form-transit');
	}

	public function formWilayah(){
		return view('form-wilayah');
	}

	public function admin(){
		return view('admin');
	}
}