<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TransportasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public $secretKey = 'a6743f1cb3ff9a545701be82f34ebc88';
    
    public function searchFlights($departure, $arrival, $date, $adult, $child, $infant)
    {
        $token = $this->getToken($this->secretKey);
        $flights = $this->getFlights($token, $departure, $arrival, $date, $adult, $child, $infant);
        return view('partials._transportasiPesawat', ['flights' => $flights]);
    }

    public function searchTrains($departure, $arrival, $date, $adult, $child)
    {
        $token = $this->getToken($this->secretKey);
        $trains = $this->getTrains($token, $departure, $arrival, $date, $adult, $child);
        return view('partials._transportasiKereta', ['trains' => $trains]);
    }

    //Mengambil token dari API tiket.com
    public function getToken($secretKey)
    {
        $url = 'https://api-sandbox.tiket.com/apiv1/payexpress?method=getToken&secretkey='.$secretKey.'&output=json';
        $jsonToken = file_get_contents($url);
        $arrToken = json_decode($jsonToken);
        return $arrToken->token;
    }

    public function getFlights($token, $departure, $arrival, $date, $adult, $child, $infant)
    {
        $url = 'http://api-sandbox.tiket.com/search/flight?d='.$departure.'&a='.$arrival.'&date='.$date.'&adult='.$adult.'&child='.$child.'&infant='.$infant.'&token='.$token.'&v=3&output=json';
        $jsonFlights = file_get_contents($url);
        $arrFlights = json_decode($jsonFlights);
        return $arrFlights;
    }

    public function getTrains($token, $departure, $arrival, $date, $adult, $child)
    {
        $url = 'https://api-sandbox.tiket.com/search/train?d='.$departure.'&a='.$arrival.'&date='.$date.'&ret_date=&adult='.$adult.'&child='.$child.'&class=all&token='.$token.'&output=json';
        $jsonTrains = file_get_contents($url);
        $arrTrains = json_decode($jsonTrains);
        return $arrTrains;
    }
}
