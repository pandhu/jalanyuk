<?php
namespace App\Http\Controllers;


use App\Models\Penginapan;
use App\Models\Tempat;
use App\Models\TempatOlehOleh;
use App\Models\TempatWisata;
use App\Models\Wilayah;
use Illuminate\Support\Facades\Request;

class SearchController extends BaseController {


	public function getSearchData()
	{

		if ($_GET['term']){
            $qSearch = $_GET['term'];
			$wilayah = Wilayah::where('nama', 'like', '%'.$qSearch.'%')->limit(7)->get();
			$wisata = Tempat::where('nama', 'like', '%'.$qSearch.'%')->limit(7)->get();
            foreach ($wilayah as $item) {
                $item->type = "wilayah";
            }
            foreach ($wisata as $item) {
                $item->type = "wisata";
            }
            $final_array = $wisata->merge($wilayah);
            return json_encode($final_array);
			//return view('search', ['wilayah'=>$wilayah, 'wisata'=>$wisata]);
		}
		
	}
    public function _searchInner($qSearch){
        $wisata = Tempat::where('nama', 'like', '%'.$qSearch.'%')->where('flag', 2)->get();
        $penginapan = Tempat::where('nama', 'like', '%'.$qSearch.'%')->where('flag', 1)->get();
        $oleh = Tempat::where('nama', 'like', '%'.$qSearch.'%')->where('flag', 0)->get();

        $markers = [];
        foreach($wisata as $item){
            $marker['lat'] = $item->lat;
            $marker['lng'] = $item->lng;
            $marker['title'] = $item->nama;
            array_push($markers, $marker);
        }
        foreach($penginapan as $item){
            $marker['lat'] = $item->lat;
            $marker['lng'] = $item->lng;
            $marker['title'] = $item->nama;
            array_push($markers, $marker);
        }
        foreach($oleh as $item){
            $marker['lat'] = $item->lat;
            $marker['lng'] = $item->lng;
            $marker['title'] = $item->nama;
            array_push($markers, $marker);
        }
        $markers = json_encode($markers);

        return view('_inner_search', ['markers'=>$markers, 'wisata'=>$wisata,'penginapan'=>$penginapan, 'oleh'=>$oleh]);
    }

}