<?php
namespace App\Http\Controllers;
use App\Models\Tempat;
use App\Models\Gallery;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Wilayah;
class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return view('home');
	}

	public function login(){
		return view('login');
	}

	public function dashboard(){
		return view('dashboard');
	}

	public function show()
    {
        $id = $_GET['id'];
        $tempat = Tempat::get();
        $zoom = 5;
        $center['lng'] = "117.796171";
        $center['lat'] = "-5.309627";
        $zoom = 4;
        $wilayah = Wilayah::find(1);
        $gallery = null;

        if($_GET['type'] == "wilayah"){
            $wisata = null;
            $gallery = null;
            $wilayah = Wilayah::find($id);
            if(!is_null($wilayah)){
                $center['lat'] = $wilayah->lat;
                $center['lng'] = $wilayah->lng;
                $zoom = 10;
            }
        }
        if($_GET['type'] == "wisata"){
            $wisata = Tempat::where('id', $id)->first();
            $gallery = Gallery::get();
            if(!is_null($wisata)){
                $wilayah = Wilayah::find($wisata->id_wilayah);
                $center['lat'] = $wisata->lat;
                $center['lng'] = $wisata->lng;
                $zoom  = 18;
            }
        }   
        return view('wilayah', ['wilayah'=>$wilayah, 
            'center' => $center, 
            'tempat' => $tempat, 
            'tempat_json' => null, 
            'gallery' => $gallery,
            'zoom' => $zoom]
            );
    }
}
