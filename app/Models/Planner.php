<?php

namespace App\Models;

use App\Models\TempatWisata;

use Illuminate\Database\Eloquent\Model;

class Planner extends Model
{
    protected $table = 'planner';



    public function tempat()
    {
    	return $this->belongsToMany('App\Models\Tempat', 'planner_tempat', 'id_planner', 'id_tempat');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'username');
    }

    public function transport()
    {
        return $this->hasMany('App\Models\Transport', 'id_planner', 'id');
    }
}
