<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function planner()
    {
        return $this->hasMany('Planner', 'id_user', 'id');
    }

    public function transportasi()
    {
        return $this->hasMany('Transportasi', 'id_user', 'id');
    }

    public function olehOleh()
    {
        return $this->hasMany('TempatOlehOleh', 'id_user', 'id');
    }

    public function penginapan()
    {
        return $this->hasMany('Penginapan', 'id_user', 'id');
    }

    public function role()
    {
        return $this->belongsToMany('Role');
    }
}
