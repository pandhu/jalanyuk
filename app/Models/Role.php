<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';

    public function user()
    {
        return $this->belongsToMany('User','role_user', 'id_role', 'id_user');
    }
}
