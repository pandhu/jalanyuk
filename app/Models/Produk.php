<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';

    public function tempat()
    {
    	return $this->belongsTo('App\Models\Tempat', 'id_tempat', 'id');
    }
}
