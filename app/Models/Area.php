<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'area';

    public function wilayah()
    {
    	return $this->belongsTo('App\Models\Wilayah', 'id_wilayah', 'id')
    }
}
