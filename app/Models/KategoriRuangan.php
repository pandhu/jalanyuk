<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriRuangan extends Model
{
    protected $table = 'kategori_ruangan';

    public function tempat()
    {
    	return $this->belongTo('App\Models\Tempat', 'id_tempat', 'id');
    }
}
