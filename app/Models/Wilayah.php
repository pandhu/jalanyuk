<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wilayah extends Model
{
    protected $table = 'wilayah';

    // by default primary key is 'id'

    public function index()
    {
    	# code...
    }

    public function parent()
    {
    	return $this->hasMany('App\Models\Wilayah', 'id_parent', 'id');
    }

    public function child()
    {
    	return $this->belongsTo('App\Models\Wilayah', 'id_parent', 'id');
    }

    public function area()
    {
    	return $this->hasMany('App\Models\Area', 'id_wilayah', 'id');
    }

    public function tempat()
    {
    	return $this->hasMany('App\Models\Tempat', 'id_wilayah', 'id');
    }
}
