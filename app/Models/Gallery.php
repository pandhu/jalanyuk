<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery_wisata';

    public function tempat()
    {
    	return $this->belongsTo('App\Models\Tempat', 'id_tempat', 'id');
    }
}
