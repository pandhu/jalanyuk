<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transportasi extends Model
{
    public function planner()
    {
    	return $this->belongsTo('App\Models\Planner', 'id_planner', 'id');
    }
}
