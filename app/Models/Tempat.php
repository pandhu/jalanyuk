<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tempat extends Model
{
    protected $table = 'tempat';

    public function planner()
    {
    	return $this->belongsToMany('App\Models\Planner');
    }

    public function wilayah()
    {
    	return $this->belongsTo('App\Models\Wilayah', 'id_wilayah', 'id');
    }

    public function gallery()
    {
    	return $this->hasMany('App\Models\Gallery', 'id_tempat', 'id');
    }

    public function produk()
    {
        return $this->hasMany('App\Models\Produk', 'id_tempat', 'id');
    }

    public function kategori_ruangan()
    {
        return $this->hasMany('App\Models\KategoriRuangan', 'id_tempat', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'username');
    }
}
