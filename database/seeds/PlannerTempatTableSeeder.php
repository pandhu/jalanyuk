<?php

use Illuminate\Database\Seeder;

class PlannerTempatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('planner_tempat')->insert(['id_planner' => 1,
         	'id_tempat' => 5,
            'no_urut' => 0
         ]);

         DB::table('planner_tempat')->insert(['id_planner' => 1,
            'id_tempat' => 1,
            'no_urut' => 1
         ]);

         DB::table('planner_tempat')->insert(['id_planner' => 1,
            'id_tempat' => 2,
            'no_urut' => 2
         ]);
    }
}
