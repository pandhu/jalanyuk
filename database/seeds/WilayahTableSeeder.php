<?php
use Illuminate\Database\Seeder;

class WilayahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wilayah')->insert(['nama' => 'Yogyakarta',
        	'lat' => '-7.8730445', 
        	'lng' => '110.4242874'
        ]);
        DB::table('wilayah')->insert(['nama' => 'Jakarta',
        	'lat' => '-6.3655899', 
        	'lng' => '106.8254544'
        ]);
    }
}
