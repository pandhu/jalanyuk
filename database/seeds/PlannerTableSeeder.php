<?php

use Illuminate\Database\Seeder;

class PlannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('planner')->insert(['nama' => 'JogjaTour']);
    }
}
