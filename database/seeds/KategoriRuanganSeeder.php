<?php

use Illuminate\Database\Seeder;

class KategoriRuanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('kategori_ruangan')->insert(['id_tempat' => 11,
            'nama' => 'Standard Double',
            'harga' => 150000,
            'foto' => 'images/ruangan/11-1.jpg',
            'created_at' => New DateTime
         ]);

         DB::table('kategori_ruangan')->insert(['id_tempat' => 11,
            'nama' => 'Deluxe Double',
            'harga' => 200000,
            'foto' => 'images/ruangan/11-2.jpg',
            'created_at' => New DateTime
         ]);

         DB::table('kategori_ruangan')->insert(['id_tempat' => 12,
            'nama' => 'Economy Double',
            'harga' => 120000,
            'foto' => 'images/ruangan/12-3.jpg',
            'created_at' => New DateTime
         ]);

         DB::table('kategori_ruangan')->insert(['id_tempat' => 12,
            'nama' => 'Standard Double',
            'harga' => 250000,
            'foto' => 'images/ruangan/12-4.jpg',
            'created_at' => New DateTime
         ]);

         DB::table('kategori_ruangan')->insert(['id_tempat' => 12,
            'nama' => 'Executive Double',
            'harga' => 325000,
            'foto' => 'images/ruangan/12-5.jpg',
            'created_at' => New DateTime
         ]);

         DB::table('kategori_ruangan')->insert(['id_tempat' => 13,
            'nama' => 'Standard Twin',
            'harga' => 150000,
            'foto' => 'images/ruangan/13-6.jpg',
            'created_at' => New DateTime
         ]);

         DB::table('kategori_ruangan')->insert(['id_tempat' => 13,
            'nama' => 'Standard Double',
            'harga' => 300000,
            'foto' => 'images/ruangan/13-7.jpg',
            'created_at' => New DateTime
         ]);

         DB::table('kategori_ruangan')->insert(['id_tempat' => 14,
            'nama' => 'Standard Twin',
            'harga' => 150000,
            'foto' => 'images/ruangan/14-8.jpg',
            'created_at' => New DateTime
         ]);

         DB::table('kategori_ruangan')->insert(['id_tempat' => 14,
            'nama' => 'Deluxe Double',
            'harga' => 150000,
            'foto' => 'images/ruangan/14-9.jpg',
            'created_at' => New DateTime
         ]);

         DB::table('kategori_ruangan')->insert(['id_tempat' => 15,
            'nama' => 'Single',
            'harga' => 150000,
            'foto' => 'images/ruangan/15-10.jpg',
            'created_at' => New DateTime
         ]);

         DB::table('kategori_ruangan')->insert(['id_tempat' => 15,
            'nama' => 'Double',
            'harga' => 175000,
            'foto' => 'images/ruangan/15-11.jpg',
            'created_at' => New DateTime
         ]);
    }
}
