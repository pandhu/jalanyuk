<?php

use Illuminate\Database\Seeder;

class GalleryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gallery_wisata')->insert(['id_tempat' => 1,
            'foto' => 'images/wisata/1-1.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 1,
            'foto' => 'images/wisata/1-2.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 1,
            'foto' => 'images/wisata/1-3.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 1,
            'foto' => 'images/wisata/1-4.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 2,
            'foto' => 'images/wisata/2-5.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 2,
            'foto' => 'images/wisata/2-6.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 3,
            'foto' => 'images/wisata/3-7.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 3,
            'foto' => 'images/wisata/3-8.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 4,
            'foto' => 'images/wisata/4-9.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 4,
            'foto' => 'images/wisata/4-10.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 5,
            'foto' => 'images/wisata/5-11.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 5,
            'foto' => 'images/wisata/5-12.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 5,
            'foto' => 'images/wisata/5-13.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 6,
            'foto' => 'images/wisata/6-14.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 6,
            'foto' => 'images/wisata/6-15.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 6,
            'foto' => 'images/wisata/6-16.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 7,
            'foto' => 'images/wisata/7-17.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 7,
            'foto' => 'images/wisata/7-18.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 8,
            'foto' => 'images/wisata/8-19.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 8,
            'foto' => 'images/wisata/8-20.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 9,
            'foto' => 'images/wisata/9-21.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 9,
            'foto' => 'images/wisata/9-22.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 9,
            'foto' => 'images/wisata/9-23.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 10,
            'foto' => 'images/wisata/10-24.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 10,
            'foto' => 'images/wisata/10-25.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 11,
            'foto' => 'images/penginapan/11-26.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 11,
            'foto' => 'images/penginapan/11-27.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 11,
            'foto' => 'images/penginapan/11-28.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 12,
            'foto' => 'images/penginapan/12-29.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 12,
            'foto' => 'images/penginapan/12-30.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 12,
            'foto' => 'images/penginapan/12-31.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 13,
            'foto' => 'images/penginapan/13-32.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 13,
            'foto' => 'images/penginapan/13-33.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 13,
            'foto' => 'images/penginapan/13-34.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 14,
            'foto' => 'images/penginapan/14-35.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 14,
            'foto' => 'images/penginapan/14-36.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 14,
            'foto' => 'images/penginapan/14-37.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 15,
            'foto' => 'images/penginapan/15-38.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 15,
            'foto' => 'images/penginapan/15-39.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 15,
            'foto' => 'images/penginapan/15-40.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 16,
            'foto' => 'images/oleh-oleh/16-41.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 16,
            'foto' => 'images/oleh-oleh/16-42.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 16,
            'foto' => 'images/oleh-oleh/16-43.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 16,
            'foto' => 'images/oleh-oleh/16-44.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 17,
            'foto' => 'images/oleh-oleh/17-45.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 17,
            'foto' => 'images/oleh-oleh/17-46.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 17,
            'foto' => 'images/oleh-oleh/17-47.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 18,
            'foto' => 'images/oleh-oleh/18-48.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 18,
            'foto' => 'images/oleh-oleh/18-49.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 18,
            'foto' => 'images/oleh-oleh/18-50.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 18,
            'foto' => 'images/oleh-oleh/18-51.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 19,
            'foto' => 'images/oleh-oleh/19-52.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 19,
            'foto' => 'images/oleh-oleh/19-53.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 19,
            'foto' => 'images/oleh-oleh/19-54.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 20,
            'foto' => 'images/oleh-oleh/20-55.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 20,
            'foto' => 'images/oleh-oleh/20-56.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 20,
            'foto' => 'images/oleh-oleh/20-57.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 21,
            'foto' => 'images/oleh-oleh/21-58.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 21,
            'foto' => 'images/oleh-oleh/21-59.jpg',
            'created_at' => New DateTime
         ]);
    }
}
