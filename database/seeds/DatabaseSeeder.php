<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();

        //$this->call(UserTableSeeder::class);
        $this->call('WilayahTableSeeder');
        $this->call('TempatTableSeeder');
        $this->call('PlannerTableSeeder');
        $this->call('PlannerTempatTableSeeder');
        $this->call('KategoriRuanganSeeder');
        $this->call('GalleryTableSeeder');
        $this->call('StasiunSeeder');
        //Model::reguard();
    }
}
