<?php

use Illuminate\Database\Seeder;

class StasiunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//transit-stasiun
        DB::table('tempat')->insert(['nama' => 'Lempuyangan',
            'flag' => 3,
            'jenis' => 'Stasiun',
            'alamat' => 'Jalan Lempuyangan Yogyakarta',
            'lat' => '-7.790221',
            'lng' => '110.375340',
            'id_wilayah' => 1,
            'kode' => 'LPN',
      		'deskripsi' => 'deskripsi',
      		'created_at' => New DateTime
        ]);

        DB::table('tempat')->insert(['nama' => 'Tugu',
            'flag' => 3,
            'jenis' => 'Stasiun',
            'alamat' => 'Jalan Ps. Kembang, Gedong Tengen, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55271',
            'lat' => '-7.789393',
            'lng' => '110.363428',
            'id_wilayah' => 1,
            'kode' => 'YK',
      		'deskripsi' => 'deskripsi',
      		'created_at' => New DateTime
        ]);

        DB::table('tempat')->insert(['nama' => 'Pasar Senen',
            'flag' => 3,
            'jenis' => 'Stasiun',
            'alamat' => 'Jalan Let. Jen. Suprapto-Kramat Bunder, Senen, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10410',
            'lat' => '-6.175109',
            'lng' => '106.844824',
            'id_wilayah' => 2,
            'kode' => 'PSE',
      		'deskripsi' => 'deskripsi',
      		'created_at' => New DateTime
        ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 25,
            'foto' => 'images/transit/25-LPN.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 26,
            'foto' => 'images/transit/26-YK.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 27,
            'foto' => 'images/transit/27-PSE.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 22,
            'foto' => 'images/transit/22-JOG.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 23,
            'foto' => 'images/transit/23-CGK.jpg',
            'created_at' => New DateTime
         ]);

        DB::table('gallery_wisata')->insert(['id_tempat' => 24,
            'foto' => 'images/transit/24-HLP.jpg',
            'created_at' => New DateTime
         ]);
    }
}
