<?php

use Illuminate\Database\Seeder;

class TempatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Tempat wisata
         DB::table('tempat')->insert(['nama' => 'Parangtritis',
            'flag' => 2,
            'jenis' => 'Pantai',
            'alamat' => 'Desa Parangtritis, Kecamatan Kretek, Kabupaten Bantul Yogyakarta.',
            'deskripsi' => 'Pantai Parangtritis merupakan obyek wisata pantai yang sangat terkenal diantara pantai-pantai lain yang tersebar di wilayah Yogyakarta ini. Pantai ini diyakini oleh masyarakat setempat sebagai perwujudan kesatuan dari Gunung Merapi, Keraton Jogja dan Parangtritis sendiri.',
            'keunikan' => 'Parangtritis memiliki pemandangan yang unik yang tidak terdapat pada obyek wisata lain, yaitu pantai yang memiliki ombak yang besar dan terdapatnya gunung-gunung pasir disektar kawasan pantai tersebut yang disebut dengan gumuk.',
            'wahana' => 'Naik Andong/Delman, Persewaan ATV, Pemandian Parang Wedang',
            'lat' => '-8.024150',
            'lng' => '110.328724',
            'harga_tiket_masuk' => 3000,
            'id_wilayah' => 1,
            'created_at' => New DateTime
         ]);

         DB::table('tempat')->insert(['nama' => 'Prambanan',
            'flag' => 2,
            'jenis' => 'Candi',
            'alamat' => 'Kecamatan Prambanan, Daerah Istimewa Yogyakarta, Indonesia',
            'deskripsi' => 'Candi Prambanan merupakan candi hindu yang dibangun pada abad ke – 9. Candi yang dikelilingi taman indah ini pernah hilang tertutup material vulkanik Gunung Merapi sebelum akhirnya ditemukan kembali dan dipugar menjadi seperti sekarang. Harga Tiket Masuk : Dewasa = Rp. 30.000,- , Anak-anak = Rp. 12.500,- , Turis Mancanegara = USD 18',
            'keunikan' => 'Candi Prambanan sudah terdaftar dalam UNESCO sebagai warisan budaya dunia. Candi Prambanan adalah Candi Hindu yang menjulang setinggi 47 meter, Candi Prambanan laksana menara kokoh yang memikat mata dunia.',
            'wahana' => 'Kompleks Candi Prambanan memiliki 3 candi utama yang menghadap ke timur, yakni Candi Wisnu, Candi Brahma, dan Candi Siwa. Setiap candi utama memiliki candi pendamping yang menghadap ke arah barat. Selain candi utama dan candi pendamping, di halaman utama juga terdapat 2 candi apit, 4 candi kelir, dan 4 candi sudut. Sementara itu di halaman kedua terdapat 224 candi.',
            'lat' => '-7.751643',
            'lng' => '110.491995',
            'harga_tiket_masuk' => 30000,
            'id_wilayah' => 1,
            'created_at' => New DateTime
         ]);

        DB::table('tempat')->insert(['nama' => 'Malioboro',
            'flag' => 2,
            'jenis' => 'Wisata Traditional',
            'alamat' => 'Jalan Malioboro, Kota Yogyakarta Deaerah Istimewa Yogyakarta.',
            'deskripsi' => 'Malioboro merupakan kawasan perbelanjaan yang legendaris yang menjadi salah satu kebanggaan kota Yogyakarta. Penamaan Malioboro berasal dari nama seorang anggota kolonial Inggris yang dahulu pernah menduduki Jogja pada tahun 1811 – 1816 M yang bernama Marlborough',
            'keunikan' => 'Banyak terdapat souvenir dan oleh oleh. Pembeli bisa menawar barang tersebut. Wisatawan juga dapat menyaksikan kekhasan lain dari Malioboro seperti puluhan andong dan becak yang parkir berderet disebelah kanan jalan pada jalur lambat Malioboro.',
            'wahana' => 'Beli Oleh-oleh, Naik andong, Jalan-Jalan, Wisata Kuliner',
            'lat' => '-7.792708',
            'lng' => '110.365752',
            'harga_tiket_masuk' => 0,
            'id_wilayah' => 1,
            'created_at' => New DateTime
         ]);

        DB::table('tempat')->insert(['nama' => 'Keraton Yogyakarta',
            'flag' => 2,
            'jenis' => 'Pantai',
            'alamat' => 'Jalan KH. Ahmad Dahlan No.17 Gondomanan Kota Yogyakarta, Daerah Istimewa Yogyakarta 55122',
            'deskripsi' => 'Keraton Yogyakarta merupakan obyek wisata yang paling populer dan sering dikunjungi oleh para wisatawan,baik itu wisatawan domestik maupun wisatawan luar negeri. Faktor sejarah membuat orang banyak yang datang ke kerotan yogyakarta ini. Sebab, keraton ini merupakan keraton yang masih ada hingga saat ini dan termasuk sebuah keraton di Indonesia yang paling besar dan terkenal. Harga Tiket Masuk : Tepas Keprajuritan Rp.3.000,- , Tepas Pariwisata Rp.5.000,- , Ijin membawa kamera/video Rp.1.000,- , Tiket masuk bagian dalam Keraton melalui Keben Rp.7.000,-',
            'keunikan' => 'Bisa menonton Musik Gamelan, Wayang Golek Menak, Pertunjukan Tari, Macapat, Wayang Kulit',
            'wahana' => 'Belajar kesenian Jawa, Melihat Sejarah Yogyakarta, Halaman depan Keraton berupa Alun-alun Utara Yogyakarta dan halaman belakang Keraton berupa Alun-alun Selatan Yogyakarta.',
            'lat' => '-7.801627',
            'lng' => '110.364202',
            'harga_tiket_masuk' => 7000,
            'id_wilayah' => 1,
            'created_at' => New DateTime
         ]);

        DB::table('tempat')->insert(['nama' => 'Taman Sari',
            'flag' => 2,
            'jenis' => 'Taman',
            'alamat' => 'Jalan Taman, Kraton, Yogyakarta 55133',
            'deskripsi' => 'Taman Sari merupakan  sebuah kompleks istana yang terdiri dari beberapa macam bangunan (tidak semua bangunan berada dalam air) lokasinya juga masih di dalam kawasan lingkungan Keraton Ngayogyakarta. Dan didalam bahasa Inggris dikenal dengan nama "Perfume Garden" atau "Fragrant Garden", karena banyak sekali bunga yang memiliki bau harum ditanam pada lingkungan taman ini. Harga Tiket Masuk : Umum : Rp. 3.000,- , WNA : Rp. 7.000,-',
            'keunikan' => 'Gemericik air, keindahan arsitekturnya yang kuno, dan pemandangan yang menakjubkan.',
            'wahana' => 'Terdiri dari 4 bagian: Danau buatan sebelah barat, Bangunan yang berada di sebelah selatan danau buatan antara lain Pemandian Umbul Binangun, Pasarean Ledok Sari dan Kolam Garjitawati, dan bagian sebelah timur.',
            'lat' => '-7.809801',
            'lng' => '110.358942',
            'harga_tiket_masuk' => 3000,
            'id_wilayah' => 1,
            'created_at' => New DateTime
         ]);

        DB::table('tempat')->insert(['nama' => 'Taman Pintar',
            'flag' => 2,
            'jenis' => 'Edukasi',
            'alamat' => 'Jalan Penembahan Senopati No.3, Yogyakarta',
            'deskripsi' => 'Taman Pintar Yogyakarta (TPY ) adalah salah satu wisata pendidikan atau wisata edukasi paling banyak di kunjungi di Yogyakarta. Rasanya belum lengkap bila mengunjungi kota Yogyakarta tidak menyempatkan diri bermain ke Taman Pintar  Yogyakarta bersama keluarga dan anak-anak. Pada bangunannya menampilkan nuansa modern dan tradisional yang mempunyai keindahan tersendiri. Taman ini menawarkan wahana belajar dan rekreasi yang cukup lengkap untuk anak-anak, baik usia pra sekolah sampai tingkat sekolah menengah. Pada rentang usia tersebut merupakan generasi penerus yang potensial mendapat pencerahan belajar ilmu dann tekhnologi (iptek)',
            'keunikan' => 'Taman ini menawarkan model edukasi atau pembelajaran yang memadukan konsep pendidikan dan permainan dengan media yang menarik sehingga dapat merangsang keingintahuan anak dan memancing kreativitas anak terhadap iptek.',
            'wahana' => 'Taman Pintar berisi materi yang terbagi menurut kelompok usia dan penekanan materi. Untuk kelompok usia dibagi lagi menjadi tingkat pra sekolah, taman kanak-kanak, sekolah dasar sampai sekolah menegah.',
            'lat' => '-7.798088',
            'lng' => '110.367657',
            'harga_tiket_masuk' => 0,
            'id_wilayah' => 1,
            'created_at' => New DateTime
         ]);

        DB::table('tempat')->insert(['nama' => 'Sasmitaloka',
            'flag' => 2,
            'jenis' => 'Sejarah',
            'alamat' => 'Jalan Bintaran Wetan No.3 Yogyakarta',
            'deskripsi' => 'Museum Sasmitaloka adalah salah satu museum sejarah yang ada di Yogyakarta. Dulunya merupakan tempat tinggal Jendral Sudirman',
            'keunikan' => 'Museum ini merupakan biografi kehidupan Sudirman dalam kesehariannya sewaktu mendiami rumah ini.',
            'wahana' => 'Gedung Utama ( 6 ruangan pameran ), Gedung Sayap Utara ( 3 ruangan pameran ), Gedung Sayap Selatan, dan Gedung Belakang ( Ruang X )',
            'lat' => '-7.802857',
            'lng' => '110.375313',
            'harga_tiket_masuk' => 0,
            'id_wilayah' => 1,
            'created_at' => New DateTime
         ]);

        DB::table('tempat')->insert(['nama' => 'Gembira Loka',
            'flag' => 2,
            'jenis' => 'Kebun Binatang',
            'alamat' => 'Jalan Kebun Raya No.2, Kotagede, Daerah Istimewa Yogyakarta 55171',
            'deskripsi' => 'Taman Gembiraloka ini merupakan kebun binatang satu-satunya yang dimiliki oleh Daerah Istimewa Yogyakarta.Kebun Binatang Gembira loka juga tergolong museum dalam kategori museum zoologicum atau museum satwa. Gembiraloka dalam sejarahnya termasuk museum tertua di Yogyakarta setelah Museum Sonobudoyo. Harga Tiket Masuk : Senin - Jumat = Rp. 20.000,- , Sabtu, Minggu, Hari Libur = Rp. 25.000,-',
            'keunikan' => 'Menampilkan satwa langka (mamalia, Reptilia, Aves, Amfibia, Pisces) dalam display berupa kandang berjeruji besi atau berpagar tembok atau kayu.',
            'wahana' => 'Menampilkan satwa langka dan koleksi binatang kering yang telah diawetkan, Laboratorium flora dan fauna',
            'lat' => '-7.805383',
            'lng' => '110.396826',
            'harga_tiket_masuk' => 25000,
            'id_wilayah' => 1,
            'created_at' => New DateTime
         ]);
    
        DB::table('tempat')->insert(['nama' => 'Baron',
            'flag' => 2,
            'jenis' => 'Pantai',
            'alamat' => 'Kanigoro, Daerah Istimewa Yogyakarta',
            'deskripsi' => 'Pantai yang berjarak sekitar 65 km dari pusat kota Jogja ini memiliki pesona panorama yang indah dan adalah tempat bagi Anda yang ingin menyantap aneka hidangan laut. Pantai Baron ini sesungguhnya adalah sebuah teluk dengan keberadaan dua buah bukit yang mengapitnya di sisi kiri dan kanannya.',
            'keunikan' => 'Daya pikat objek wisata pantai Jogja yang satu ini adalah pada sajian kuliner laut yang lezat.',
            'wahana' => 'Wisata Kuliner, Wahana bermain anak dan perahu mesin, Toko-toko cenderamata.',
            'lat' => '-8.126948',
            'lng' => '110.548490',
            'harga_tiket_masuk' => 10000,
            'id_wilayah' => 1,
            'created_at' => New DateTime
         ]);

        DB::table('tempat')->insert(['nama' => 'Sri Gethuk',
            'flag' => 2,
            'jenis' => 'Air Terjun',
            'alamat' => 'Bleberan, Playen, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta',
            'deskripsi' => 'Terletak di antara ngarai Sungai Oya yang dikelilingi areal persawahan nan hijau, Air Terjun Sri Gethuk selalu mengalir tanpa mengenal musim. Gemuruhnya menjadi pemecah keheningan di bumi Gunungkidul yang terkenal kering.',
            'keunikan' => 'Gemuruh airnya membawa ketenangan dan kenyamanan yang luar biasa',
            'wahana' => 'Persewaan Rakit Motor',
            'lat' => '-7.951364',
            'lng' => '110.485903',
            'harga_tiket_masuk' => 35000,
            'id_wilayah' => 1,
            'created_at' => New DateTime
         ]);

        // Penginapan
        DB::table('tempat')->insert(['nama' => 'Makuta Guest House',
            'flag' => 0,
            'alamat' => 'Jl. Prawirotaman 2 / 839  , Mergangsan, 55153 Yogyakarta, Indonesia',
            'deskripsi' => 'Makuta Guest House memiliki lokasi yang strategis di jantung kota Yogyakarta, serta berselang 5 menit berkendara dari jalan Malioboro. Akomodasi yang simpel ini menyediakan Wi-Fi gratis di seluruh area bangunan dan juga parkir di-akomodasi secara gratis. Keraton Yogyakarta dapat dicapai dalam waktu 5 menit berkendara dari Makuta Guest House, sedangkan Pantai Parangtritis dan Bandara Internasional Adisucipto berselang 20 menit berkendara. Kamar-kamarnya yang cerah menawarkan kipas angin, serta berlantai keramik dan menyediakan seprai yang segar. Seluruh kamar juga mencakup meja, lemari pakaian, dan kamar mandi pribadi. Bagasi dapat dititipkan di meja depan 24-jam, sedangkan wisata sehari dan penyewaan kendaraan dapat diatur di meja layanan wisata. Staf hotel dapat membantu dengan layanan binatu dan penjemputan bandara. Kopi dan teh gratis disuguhkan setiap hari di area lobi. Untuk makanan, terdapat banyak pilihan tempat bersantap yang berada dalam jarak berjalan kaki dari akomodasi. Mergangsan adalah pilihan tepat buat wisatawan yang suka Orang-orang yang ramah, Kuil, dan Budaya. Orang lokal juga suka Mergangsan untuk Wisata kuliner,Kuliner Khas, dan Museum. ',
            'lat' => '-7.820296',
            'lng' => '110.372716',
            'id_wilayah' => 1,
            'created_at' => New DateTime
        ]);

        DB::table('tempat')->insert(['nama' => 'Hotel Seno',
            'flag' => 0,
            'alamat' => 'Jalan Jogokaryan No. 26, Yogyakarta, Mantrijeron, 55143 Yogyakarta, Indonesia',
            'deskripsi' => 'Hotel Seno terletak di jantung kota Yogyakarta, hanya 10 menit berkendara dari Jalan Malioboro yang ramai. Hotel ini menyediakan layanan pijat, restoran, dan Wi-Fi gratis di seluruh hotel. Hotel Seno terletak sejauh 20 menit berkendara dari Bandara Adi Sucipto. Hotel juga berjarak hanya 15 menit berkendara dari Keraton Sultan. Parkir gratis tersedia di hotel. Hotel ini menyediakan akomodasi sederharna yang dilengkapi dengan perabotan tradisional Jawa dan elemen dekoratif. Semua kamar menawarkan pemandangan taman dan dilengkapi dengan kipas angin, meja, dan lemari pakaian. Beberapa kamar memiliki kamar mandi en suite, AC, TV, dan teras pribadi. Hotel menyediakan layanan binatu, penjemputan bandara, serta fasilitas penyewaan mobil. Anda juga dapat mengunjungi Batik Seno di dekat hotel. Batik Seno adalah galeri batik di mana Anda dapat menyaksikan proses pembuatan batik dan mengikuti kursus membatik. Mantrijeron adalah pilihan tepat buat wisatawan yang suka Sejarah, Orang-orang yang ramah, dan Kuil. Orang lokal juga suka Mantrijeron untuk Pantai,Wisata kuliner, dan Wisata Kota.',
            'lat' => '-7.824234',
            'lng' => '110.363489',
            'id_wilayah' => 1,
            'created_at' => New DateTime
        ]);

        DB::table('tempat')->insert(['nama' => 'Ndalem Mantrigawen',
            'flag' => 0,
            'alamat' => 'Jalan Mantrigawen Lor No. 15, Kraton, 55131 Yogyakarta, Indonesia',
            'deskripsi' => 'Ndalem Mantrigawen berlokasi di Yogyakarta, dan menawarkan Wi-Fi gratis di seluruh areanya. Akomodasi ini menawarkan taman yang menawan dan teras di mana Anda dapat bersantai. Ndalem Mantrigawen berjarak 800 meter dari Keraton Yogyakarta, 1,8 km dari Jalan Malioboro, dan 7 km dari Bandara Internasional Adi Sucipto. Kamar-kamarnya menawarkan area tempat duduk dan pemandangan taman. Anda dapat menggunakan fasilitas kamar mandi bersama atau pribadi. Fasilitas tambahan lainnya mencakup meja kerja dan seprai. Ndalem Mantrigawen menawarkan meja depan 24 jam, layanan binatu, dan layanan antar-jemput bandara dengan biaya tambahan. Tersedia TV layar datar di ruang tamu bersama. Sarapan dapat dinikmati dalam kenyamanan kamar Anda. Kami berbicara bahasa Anda! Kamar Hotel: 13 Akomodasi ini sudah ada di ',
            'lat' => '-7.808864',
            'lng' => '110.368779',
            'id_wilayah' => 1,
            'created_at' => New DateTime
        ]);
        
        DB::table('tempat')->insert(['nama' => 'Ndalem Mbak Charly',
            'flag' => 0,
            'alamat' => 'Jl. Nuri no. 6, Demangan, Gejayan, Catur Tunggal, 55122 Yogyakarta, Indonesia',
            'deskripsi' => 'Ndalem Mbak Charly berjarak dekat dengan deretan factory outlet, menawarkan akomodasi berharga terjangkau dengan akses Wi-Fi gratis di seluruh areanya. Akomodasi ini memiliki bagian penerima tamu 24 jam dan meja layanan wisata. Tersedia tempat parkir gratis untuk Anda yang membawa kendaraan. Ndalem Mbak Charly berjarak 5 menit berkendara dari Ambarrukmo Plaza dan Galeria Mall, sedangkan stasiun Tugu dan jalan Malioboro berselang 10 menit berkendara. Bandara internasional Adi Sucipto dapat dicapai dengan 15 menit perjalanan naik mobil. Setiap kamar memiliki kipas angin atau AC, serta dilengkapi meja, TV layar datar, dan handuk bersih. Tersedia kamar mandi bersama dengan shower dan perlengkapan mandi gratis. Hidangan khas lokal di sajikan untuk sarapan setiap hari. Ndalem Mbak Charly menyediakan layanan binatu, penyewaan mobil, dan antar-jemput bandara. Sekarang, Anda juga dapat menggunakan kolam renang outdoor milik hotel di dekatnya dengan biaya tambahan. Hidangan dapat dipesan melalui layanan kamar. Selain itu, Anda juga dapat memilih berbagai restoran dan kafe yang berselang 5 menit jalan kaki. Catur Tunggal adalah pilihan tepat buat wisatawan yang suka Kuliner Khas, Wisata kuliner, dan Kuliner. Orang lokal juga suka Catur Tunggal untuk Kuliner Khas,Wisata kuliner, dan Wisata Kota. ',
            'lat' => '-7.779406',
            'lng' => '110.390925',
            'id_wilayah' => 1,
            'created_at' => New DateTime
        ]);

        DB::table('tempat')->insert(['nama' => 'Joglo Aruna',
            'flag' => 0,
            'alamat' => 'Jalan Bugisan Selatan (kantor pos bugisan), 55182 Yogyakarta, Indonesia',
            'deskripsi' => 'Dengan Wi-Fi gratis di seluruh properti, Joglo Aruna menawarkan akomodasi di Yogyakarta, hanya 2,5 km dari Museum Sonobudoyo. Tersedia juga TV layar datar. Terdapat dapur bersama di properti.Istana Kepresidenan Yogyakarta berjarak 2.6 km dari Joglo Aruna, sedangkan Benteng Vredeburg terletak 2.9 km dari properti. Bandara yang terdekat adalah Bandara Internasional Adisucipto yang berjarak 10 km dari hotel.',
            'lat' => '-7.817482',
            'lng' => '110.347395',
            'id_wilayah' => 1,
            'created_at' => New DateTime
        ]);

        // Tempat Oleh Oleh
        DB::table('tempat')->insert(['nama' => 'Grosir Oleh Oleh',
            'flag' => 1,
            'alamat' => 'Warungboto UH IV / No. 696, Umbulharjo',
            'deskripsi' => 'Pusat Grosir Oleh Oleh Indonesia di seluruh Nusantara. Kami menyediakan Oleh Oleh khas dari daerah Indonesia dari Sabang hingga Merauke. Grosiroleholeh.com merupakan salah satu produk dari CV. Griya Media Nusantara yang berada di Yogyakarta.',
            'keunikan' => 'Menyediakan Oleh Oleh khas dari daerah Indonesia dari Sabang hingga Merauke, mempunyai online store',
            'wahana' => 'Beli aneka oleh-oleh, kerajinan tangan',
            'lat' => '-7.8106229',
            'lng' => '110.3866479',
            'harga_tiket_masuk' => 0,
            'id_wilayah' => 1
        ]);

        DB::table('tempat')->insert(['nama' => 'Pasar Beringharjo',
            'flag' => 1,
            'alamat' => 'Jalan Pabringan, Gondomanan, Kota Yogyakarta',
            'deskripsi' => 'Pasar tradisional ini usianya hampir sama dengan Kraton Yogyakarta yang berdiri sejak 1758. Selama berabad-abad, kawasan ini mejadi pusat ekonomi kota Jogja sampai menjadi pasar yang kamu kenal sekarang ini.',
            'keunikan' => 'Menjual berbagai barang, mulai dari aneka macam kain dan busana batik, jajanan pasar, bahan makanan, jamu-jamuan, gadget, sampai barang-barang antik dan pakaian dalam. Harga relatif murah',
            'wahana' => 'Pasar',
            'lat' => '-7.7990006',
            'lng' => '110.3670994',
            'harga_tiket_masuk' => 0,
            'id_wilayah' => 1
        ]);

        DB::table('tempat')->insert(['nama' => 'Pasar Klithikan',
            'flag' => 1,
            'alamat' => 'Jalan Hos. Cokroaminoto, Wirobrajan',
            'deskripsi' => 'Di pasar ini, kamu bisa menemukan beragam barang-barang bekas dan baru, seperti onderdil dan aksesoris kendaraan bermotor, barang-barang antik nan jadul, pakaian, serta ponsel baru dan bekas, mulai dari yang keluaran terbaru sampai yang lawas. ',
            'keunikan' => 'Menjual barang-barang antik dan jadul.',
            'wahana' => 'Pasar barang antik',
            'lat' => '-7.7972637',
            'lng' => '110.3532091',
            'harga_tiket_masuk' => 0,
            'id_wilayah' => 1
        ]);

        DB::table('tempat')->insert(['nama' => 'Cokelat Monggo',
            'flag' => 1,
            'alamat' => 'Jalan Dalem KG III / 978, Purbayan Kotagede',
            'deskripsi' => 'MONGGO CHOCOLATE is produced in Indonesia and prepared with the utmost respect for the tradition of the great master chocolatiers. All Monggo products are made of the finest dark chocolate with 100% cocoa butter.',
            'keunikan' => 'Menjual cokelat khas dan beberapa suvenir lain',
            'wahana' => 'Toko Oleh-oleh, makanan & cokelat',
            'lat' => '-7.8316019',
            'lng' => '110.3994024',
            'harga_tiket_masuk' => 0,
            'id_wilayah' => 1
        ]);

        DB::table('tempat')->insert(['nama' => 'Dagadu Djokdja',
            'flag' => 1,
            'alamat' => 'Jalan Gedongkuning Sel.Kotagede',
            'deskripsi' => 'Sejak awal kelahirannya, Dagadu Djokdja memposisikan diri sebagai produk cinderamata alternatif dari Djokdja dengan mengusung tema utama: Everything about Djokdja. Ya artefaknya, bahasanya, kultur kehidupannya, maupun remeh-temeh keseharian yang terjadi di dalamnya. Terminologi “alternatif” digunakan untuk membedakan produk Dagadu Djokdja dengan cinderamata lain dengan karakteristik : memberi bingkai estetika pada hal-hal keseharian yang dianggap sederhana dan remeh; mengungkapkan gagasan dengan gaya bermain-main yang mudah dipahami; memberi penekanan pada aspek keatraktifan melalui bentuk-bentuk sederhana yang mencolok; memilih fabrikan ketimbang citra craft atau kerajinan, baik melalui material yang digunakan maupun unsur-unsur desain dari pemilihan warna hingga finishing.',
            'keunikan' => 'Menjual suvenir khas Dagadu Jogja',
            'wahana' => 'Toko oleh-oleh, suvenir, & pakaian',
            'lat' => '-7.8149287',
            'lng' => '110.4018336',
            'harga_tiket_masuk' => 0,
            'id_wilayah' => 1
        ]);

        DB::table('tempat')->insert(['nama' => 'Bakpia Pathok 25',
            'flag' => 1,
            'alamat' => 'Jalan Mataram',
            'deskripsi' => 'Apa itu Bakpia Pathok 25?  Bakpia kita kenal sebagai makanan khas Yogyakarta, hampir setiap toko oleh - oleh yang tersebar di seluruh Daerah Istimewa Yogyakarta menyediakan bakpia untuk dibeli. ',
            'keunikan' => 'Menjual bakpia pathok khas jogja',
            'wahana' => 'Toko oleh-oleh, suvenir, & makanan',
            'lat' => '-7.7940619',
            'lng' => '110.3677683',
            'harga_tiket_masuk' => 0,
            'id_wilayah' => 1
        ]);

        //Transit
        DB::table('tempat')->insert(['nama' => 'Adi Sutjipto',
            'flag' => 3,
            'jenis' => 'Bandara',
            'alamat' => 'Yogyakarta',
            'deskripsi' => 'desc',
            'lat' => '-7.787652',
            'lng' => '110.431751',
            'id_wilayah' => 1,
            'kode' => 'JOG',
            'id_negara' => 'id',
            'nama_negara' => 'Indonesia'
        ]);
        DB::table('tempat')->insert(['nama' => 'Soekarno Hatta',
            'flag' => 3,
            'jenis' => 'Bandara',
            'alamat' => 'Jakarta',
            'deskripsi' => 'desc',
            'lat' => '-6.127522',
            'lng' => '106.653697',
            'id_wilayah' => 2,
            'kode' => 'CGK',
            'id_negara' => 'id',
            'nama_negara' => 'Indonesia'
        ]);
        DB::table('tempat')->insert(['nama' => 'Halim Perdanakusuma',
            'flag' => 3,
            'jenis' => 'Bandara',
            'alamat' => 'Jakarta',
            'deskripsi' => 'desc',
            'lat' => '-6.265180',
            'lng' => '106.890860',
            'id_wilayah' => 2,
            'kode' => 'HLP',
            'id_negara' => 'id',
            'nama_negara' => 'Indonesia'
        ]);
    }
}
