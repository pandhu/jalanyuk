<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('area', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lng');
            $table->string('lat');
            $table->integer('id_wilayah')->unsigned();
            $table->foreign('id_wilayah')->references('id')->on('wilayah');
            $table->timestamps();
        });    
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('area');
    }
}
