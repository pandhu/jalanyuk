<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKategoriRuanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('kategori_ruangan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tempat')->unsigned();
            $table->foreign('id_tempat')->references('id')->on('tempat');
            $table->string('nama');
            $table->integer('harga');
            $table->string('foto');
            $table->timestamps();
        });    
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kategori_ruangan');
    }
}
