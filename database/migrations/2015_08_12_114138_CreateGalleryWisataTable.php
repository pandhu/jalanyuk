<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryWisataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('gallery_wisata', function (Blueprint $table) {
            $table->increments('id_foto');
            $table->integer('id_tempat')->unsigned();
            $table->foreign('id_tempat')->references('id')->on('tempat');
            $table->string('foto');
            $table->timestamps();
        });    
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery_wisata');
    }
}
