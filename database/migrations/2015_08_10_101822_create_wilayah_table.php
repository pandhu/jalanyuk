<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWilayahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('wilayah', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('lng');
            $table->string('lat');
            $table->integer('id_parent')->unsigned()->nullable();
            $table->foreign('id_parent')->references('id')->on('wilayah');
            $table->timestamps();
        });    
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wilayah');
    }
}
