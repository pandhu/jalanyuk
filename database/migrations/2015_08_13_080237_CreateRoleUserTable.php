<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_user', function(Blueprint $table){
            $table->integer('id_role')->unsigned();
            $table->foreign('id_role')->references('id')->on('role');
            $table->string('id_user');
            $table->foreign('id_user')->references('username')->on('user');
            $table->primary(['id_role', 'id_user']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('role_user');
    }
}
