<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('tempat', function (Blueprint $table) {
            //General
            $table->increments('id');
            $table->integer('flag');
                // Note Flag
                // 0 = Pengingapan
                // 1 = Tempat Oleh Oleh
                // 2 = Tempat Wisata
                // 3 = Transit
            $table->string('nama');
            $table->string('jenis');
            $table->string('alamat');
            $table->text('deskripsi');
            $table->string('lng');
            $table->string('lat');
            $table->integer('id_wilayah')->unsigned();
            $table->foreign('id_wilayah')->references('id')->on('wilayah');
            $table->string('id_user')->nullable();
            $table->foreign('id_user')->references('username')->on('user');
            $table->timestamps();

            //Tempat Wisata
            $table->text('keunikan')->nullable();
            $table->text('wahana')->nullable();
            $table->integer('harga_tiket_masuk')->nullable();

            //Transit
            $table->string('kode')->nullable();
            $table->string('id_negara')->nullable();
            $table->string('nama_negara')->nullable();
        });    
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tempat');
    }
}
