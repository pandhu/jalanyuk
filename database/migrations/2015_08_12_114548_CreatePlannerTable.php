<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlannerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('planner', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->date('tanggal_berlibur')->nullable();
            $table->string('id_user')->nullable();
            $table->foreign('id_user')->references('username')->on('user');
            $table->timestamps();
        });    
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('planner');
    }
}
