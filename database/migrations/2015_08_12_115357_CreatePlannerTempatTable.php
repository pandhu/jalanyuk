<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlannerTempatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('planner_tempat', function (Blueprint $table) {
            $table->integer('id_planner')->unsigned();
            $table->foreign('id_planner')->references('id')->on('planner');
            $table->integer('id_tempat')->unsigned();
            $table->foreign('id_tempat')->references('id')->on('tempat');           
            $table->primary(['id_planner', 'id_tempat']);
            $table->integer('no_urut');
            $table->timestamps();
        });    
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('planner_tempat');
    }
}
