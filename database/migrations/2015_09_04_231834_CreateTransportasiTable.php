<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportasi', function(Blueprint $table){
            $table->increments('id');
            $table->integer('flag');
            $table->text('json');
            $table->integer('id_planner')->unsigned();
            $table->foreign('id_planner')->references('id')->on('planner');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transportasi');
    }
    
}
