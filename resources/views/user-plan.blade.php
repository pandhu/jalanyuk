@extends('layout.master')

@section('master-content')

<section class="user-content">
	<div class="container">
	    <div class="row">
	    	<h2 class="admin-title">Rencana Saya</h2>
	        <div class="col-md-6">
	            <!-- begin panel group -->
	            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    @for($ii =0 ; $ii < $divider; $ii++)
	                <!-- panel 1 -->
	                <div class="panel panel-default panel-user">
	                    <!--wrap panel heading in span to trigger image change as well as collapse -->
	                    <span class="side-tab" data-target="#tab{{$ii}}" data-toggle="tab" role="tab" aria-expanded="false">
	                        <div class="panel-heading panel-user__head" role="tab" id="headingOne"data-toggle="collapse" data-parent="#accordion" href="#collapse{{$ii}}" aria-expanded="true" aria-controls="collapseOne">
	                            <div class="row">
	                            	<div class="col-md-6">
		                            	<h4 class="panel-user__title">{{$plans[$ii]->nama}}</h4>
		                            </div>
		                            <div class="col-md-3">
		                            	
		                            </div>

	                            </div>
	                        </div>
	                    </span>
	                    <div id="collapse{{$ii}}" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
	                        <div class="panel-body panel-user__body">
	                        <!-- Tab content goes here -->
	                        @foreach($plans[$ii]->tempat()->get() as $item)
                            {{$item->nama}}<br/>
                            @endforeach
                                <div class="col-md-3 pull-right">
                                    <button type="button" data-id="{{$plans[$ii]->id}}" class="btn btn-default btn-admin btn-planner-delete" data-toggle="tooltip" data-placement="bottom" title="Hapus Rencana">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </button>
                                    <a href="{{url('planner/'.$plans[$ii]->id)}}" type="button" class="btn btn-default btn-admin" data-toggle="tooltip" data-placement="bottom" title="Lihat Rencana">
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a>
                                </div>
	                        </div>
	                    </div>
	                </div> 
	                <!-- / panel 1 -->
                    @endfor


                </div> <!-- / panel-group -->
	             
	        </div> <!-- /col-md-6 -->

	        <div class="col-md-6">
	            <!-- begin panel group -->
	            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    @for($ii =$divider ; $ii < count($plans); $ii++)
                        <!-- panel 1 -->
                        <div class="panel panel-default panel-user">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
	                    <span class="side-tab" data-target="#tab{{$ii}}" data-toggle="tab" role="tab" aria-expanded="false">
	                        <div class="panel-heading panel-user__head" role="tab" id="headingOne"data-toggle="collapse" data-parent="#accordion" href="#collapse{{$ii}}" aria-expanded="true" aria-controls="collapseOne">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="panel-user__title">{{$plans[$ii]->nama}}</h4>
                                    </div>
                                    <div class="col-md-3">

                                    </div>
                                </div>
                            </div>
	                    </span>
                            <div id="collapse{{$ii}}" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body panel-user__body">
                                    <!-- Tab content goes here -->
                                    @foreach($plans[$ii]->tempat()->get() as $item)
                                        {{$item->nama}}<br/>
                                    @endforeach
                                    <div class="col-md-3 pull-right">
                                        <button type="button" data-id="{{$plans[$ii]->id}}" class="btn btn-default btn-admin btn-planner-delete" data-toggle="tooltip" data-placement="bottom" title="Hapus Rencana">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </button>
                                        <a href="{{url('planner/'.$plans[$ii]->id)}}" type="button" class="btn btn-default btn-admin" data-toggle="tooltip" data-placement="bottom" title="Lihat Rencana">
                                            <i class="glyphicon glyphicon-eye-open"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 1 -->
                        @endfor

	            </div> <!-- / panel-group -->
	             
	        </div> <!-- /col-md-6 -->
	        
	        
	    </div> <!--/ .row -->
	</div> <!-- end sidetab container -->
</section>

@endsection
@section('master-js')
<script>
    console.log("ready");
    $(document).ready(function(){
        $(document).on('click', '.btn-planner-delete', function(){
            //console.log("aa");
            var id_planner = $(this).attr('data-id');
            //console.log(id_planner);

            $.ajax({
                method: "POST",
                url: "{{url('planner/delete')}}",
                data: { 'id_planner': id_planner },
            })
                    .done(function( html ) {
                        //console.log("bb");
                        location.reload();
                    });
        });
    });
</script>
@endsection