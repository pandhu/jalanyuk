@extends('layout.master')

@section('master-content')
    <section class="no-padding map-content">

        <div class="map-content no-padding">
            <div id="map_canvas"></div>
        </div>

        <div class="toggle-btn-left" >
            <a class="btn btn-default btn-sidebar" data-toggle="tooltip" data-placement="right" title="Daftar Tempat">
                <i class="fa fa-car fa-2x"></i>
            </a>
        </div>

        <div id="left_bar" class="left-sidebar col-md-4 no-padding">
            <div class="left-sidebar__top">
                <div class="tabbable-panel">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_wisata" data-toggle="tab">
                                <i class="fa fa-bicycle"></i> Wisata </a>
                            </li>
                            <li>
                                <a href="#tab_penginapan" data-toggle="tab">
                                <i class="fa fa-building"></i> Penginapan </a>
                            </li>
                            <li>
                                <a href="#tab_oleh" data-toggle="tab">
                                <i class="fa fa-shopping-cart"></i> Oleh-oleh </a>
                            </li>
                        </ul>
                        <div class="tab-content sidebar-content">
                            <div class="tab-pane active" id="tab_wisata">
                                <ul class="dest">

                                </ul>
                            </div>
                            <div class="tab-pane" id="tab_penginapan">
                                <ul class="dest">

                                </ul>
                            </div>
                            <div class="tab-pane" id="tab_oleh">
                                <ul class="dest">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="left-sidebar__bottom">
                <div class="fixed-search">
                    <div class="input-group col-md-12">
                        <input type="text" class="form-control input-lg input-search" placeholder="Buscar" />
                        <span class="input-group-btn">
                            <button class="btn btn-info btn-lg btn-search" type="button">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="detail_tempat">

        </div>   

        <div class="toggle-btn-right">
            <a class="btn btn-default btn-sidebar" data-toggle="tooltip" data-placement="left" title="Rencana Wisata">
                <i class="fa fa-pencil-square-o fa-2x"></i>
            </a>
        </div>

        <div id="right_bar" class="right-sidebar col-md-4 no-padding">
            <div class="planner">          
                <div class="planner-title">
                    <h4 class="text-center">
                        <i class="fa fa-briefcase"></i> Rencana Wisata
                    </h4>
                </div> 
                <div class="col-md-12 bhoechie-tab-container" id="tab_transit">
                    
                </div>
                <ul class="dest planner-result">


                    
                </ul>
                <!-- Kalo belum ada plan yang ditambahin, 2 tombol ini jangan dimunculin-->
                <div class="plan-submit hidden" id="save_plan">

                    <div class="col-md-1 no-padding"></div>
                    @if (Auth::check())
                        <div class="col-md-4 no-padding">
                            <button class="btn btn-default btn-plan" data-toggle="modal" data-target=".save-plan"><i class="fa fa-floppy-o"></i> Simpan Rencana</button>
                        </div>
                        <div class="col-md-2 no-padding">
                            
                        </div>
                        <div class="col-md-4 no-padding">
                 <button class="btn btn-default btn-plan"><i class="fa fa-download"></i> Unduh Rencana</button>

                            <button class="btn btn-default btn-plan-analisis"><i class="fa fa-download"></i> Analisa Rencana</button>
                        </div>
                    @else
                        <div class="col-md-10 no-padding">
                            <center> Login untuk menyimpan rencana </center>
                        </div>
                    @endif
                    <div class="col-md-1 no-padding"></div>
                </div>
            </div>
        </div>
        
        <div class="modal fade save-plan" tabindex="-1" role="dialog" aria-labelledby="myMediumModalLabel">
            <div class="modal-dialog modal-md">
                <div class="modal-content modal-custom">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Simpan Rencana Wisata</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-inline">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Buat nama rencana</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control modal-custom input-planner-name" placeholder="Nama rencana">
                                </div>
                                
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-custom" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-warning modal-custom btn-planner-save" id="confirm">Simpan</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade success-save" tabindex="-1" role="dialog" aria-labelledby="myMediumModalLabel">
            <div class="modal-dialog modal-md">
                <div class="modal-content modal-custom">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Selamat</h4>
                    </div>
                    <div class="modal-body">
                                <h4 class="text-center">Rencana Berhasil Disimpan!</h4>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-custom" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade detail-transport" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content modal-custom">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Pilih Jadwal</h4>
                    </div>
                    <div class="modal-body" id="tab_transportasi">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade alert-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content modal-custom">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title alert-modal-title"></h4>
                    </div>
                    <div class="modal-body alert-modal-body" id="">


                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade detail-transport" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content modal-custom">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Pilih Jadwal</h4>
                    </div>
                    <div class="modal-body" id="tab_transportasi">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade alert-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content modal-custom">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title alert-modal-title"></h4>
                    </div>
                    <div class="modal-body alert-modal-body" id="">

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade general-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="general_modal_lg">
            
        </div>

        <div class="modal fade loading-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="loading_modal_lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content modal-custom">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Title</h4>
                    </div>
                    <div class="modal-body" id="tab_transportasi">
                        Loading...
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('master-js')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4gMP99taarZnZl4J-BxPGGyw2py8u12U&libraries=places"></script>
    <!-- <script src="{{asset('js/script.js')}}"></script>
    -->
    <!--
    <script src="{{asset('js/wilayahScript.js')}}"></script>
    -->

    <script>
        var map;
        var directionsService;
        var directionsDisplay;
        var markers = [];
        var directionItem = [];
        var wayPoints = [];
        var responses;
        var isCustomPick = false;
        var latt;
        var lngg;

        var instructions= [];

//---------------------------------------------------------------------------------------------------------
//----------------------------------------------Marker-----------------------------------------------------

        // Adds a marker to the map and push to the array.
        function addMarker(location, title, image, flag, id) {
            /*
            var contentString = '<div id="content">'+
              '<div id="siteNotice">'+
              '</div>'+
              '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
              '<div id="bodyContent">'+
              '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
              'sandstone rock formation in the southern part of the '+
              'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
              'south west of the nearest large town, Alice Springs; 450&#160;km '+
              '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
              'features of the Uluru - Kata Tjuta National Park. Uluru is '+
              'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
              'Aboriginal people of the area. It has many springs, waterholes, '+
              'rock caves and ancient paintings. Uluru is listed as a World '+
              'Heritage Site.</p>'+
              '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
              'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
              '(last visited June 22, 2009).</p>'+
              '</div>'+
              '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
              });
            */
            var marker = new google.maps.Marker({
                position: location,
                map: map,
                animation: google.maps.Animation.DROP,
                title : title,
                icon : image
            });

            marker.addListener('click', function() {
                setModalTempat('{{url("detail/wisata/")}}/'+flag+'/'+id);
                //infowindow.open(map, marker);
              });

            markers[id] = marker;
        }
        function addMarkers(markers){
            markers.forEach(function (entry) {
                //console.log(entry);
                createMarker(parseFloat(entry.lat),parseFloat(entry.lng), entry.title, entry.flag, entry.id);
            });
        }
        // Sets the map on all markers in the array.
        function setMapOnAll(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setMapOnAll(null);
        }

        // Shows any markers currently in the array.
        function showMarkers() {
            setMapOnAll(map);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }

        function createMarker(lat, lng, title, flag, places, id){
            var location = {lat: lat, lng: lng};
            var image = "{{asset('images/pin/pin_selected_24x32.png')}}";
            //console.log(places);
            if (flag == 0){
                image = "{{asset('images/pin/pin_tempat-wisata_24x32.png')}}";
            } else if(flag == 1){
                image = "{{asset('images/pin/pin_oleh-oleh_24x32.png')}}";
            } else if (flag == 2){
                image = "{{asset('images/pin/pin_tempat-wisata_24x32.png')}}";
            } else if (flag == 3){
                image = "{{asset('images/pin/pin_transit_24x32.png')}}";
            }
            addMarker(location, title, image, flag, id);
        }

        function setMarkerIcon(id, flag, isSelected){
            var image = "{{asset('images/pin/pin_selected_24x32.png')}}";
            if(markers.length > 0) {
                if(isSelected){
                    image ="{{asset('images/pin/pin_selected_24x32.png')}}";
                } else {
                    if (flag == 0){
                        image = "{{asset('images/pin/pin_tempat-wisata_24x32.png')}}";
                    } else if(flag == 1){
                        image = "{{asset('images/pin/pin_oleh-oleh_24x32.png')}}";
                    } else if (flag == 2){
                        image = "{{asset('images/pin/pin_tempat-wisata_24x32.png')}}";
                    } else if (flag == 3){
                        image = "{{asset('images/pin/pin_transit_24x32.png')}}";
                    }
                }
                markers[id].setIcon(image);
            }
        }

//---------------------------------------------------------------------------------------------------------
//----------------------------------------------Direction--------------------------------------------------

        function makeDirection(places){
            removeDirection();           
            var last = places.length - 1;
            wayPoints = [];
            for (var i = 1; i < places.length-1; i++) {
              wayPoints.push({
                 location: new google.maps.LatLng(places[i]['lat'],places[i]['lng']),
                 stopover:true
              }); 
            };

            request = {
                origin: new google.maps.LatLng(places[0]['lat'],places[0]['lng']),
                destination: new google.maps.LatLng(places[last]['lat'],places[last]['lng']),
                waypoints: wayPoints,
                optimizeWaypoints: true,
                provideRouteAlternatives: false,
                durationInTraffic: true,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    responses = response;

                    //console.log(responses);

                    renderAttribute();
                    
                }
            });
            directionsDisplay.setMap(map);
            
        }

        function removeDirection(){
            directionsDisplay.setMap(null);
        }

        function renderAttribute(){
            var jarak = 0;
            var waktu = 0;
            var jsonPlanner = JSON.parse(localStorage['planner']);
            //console.log(jsonPlanner);
            //console.log(responses);
            
            if(responses != null){
                for (var i = 1; i < jsonPlanner.length; i++) {
                    jsonPlanner[i]
                    jarak = responses["routes"][0]["legs"][i-1]["distance"]["text"];
                    waktu = responses["routes"][0]["legs"][i-1]["duration"]["text"];
                    //console.log(responses["routes"][0]["legs"][i-1]);
                    instructions = [];
                    for ( var j= 0; j< responses["routes"][0]["legs"][i-1]["steps"].length; j++){
                        // console.log(responses["routes"][0]["legs"][i-1]["steps"][j]["instructions"]);
                        instructions.push(responses["routes"][0]["legs"][i-1]["steps"][j]["instructions"]);
                    }
                    document.getElementById('route '+jsonPlanner[i]['id']).innerHTML = '<p class="jarak"><i class="fa fa-street-view"></i>'+jarak+'</p><p class="waktu"><i class="fa fa-clock-o"></i>'+waktu+'</p>';
                };
            } else {
                jarak = 0;
                waktu = 0;
            }
        }
//
//---------------------------------------------------------------------------------------------------------
//-------------------------------------------------Init----------------------------------------------------
        function initialize() {

            //Manipulasi style google maps : hide default marker
            var myStyles =[
                {
                    featureType: "poi",
                    elementType: "labels",
                    stylers: [
                          { visibility: "off" }
                    ]
                }
            ];
            var mapOptions = {
                zoom: {{$zoom}},
                styles: myStyles
            };

            map = new google.maps.Map(document.getElementById('map_canvas'),  mapOptions);

            directionsService = new google.maps.DirectionsService();
            directionsDisplay = new google.maps.DirectionsRenderer({
                //Hide direction marker
                suppressMarkers: true
            });

            @foreach($tempat as $item)
                createMarker({{$item->lat}}, {{$item->lng}},"{{$item->nama}}", {{$item->flag}}, "{{$item}}", {{$item->id}});
            @endforeach
            createDirection();
            setCenter({{$center['lat']}},{{$center['lng']}});

            
            google.maps.event.addListener(map, 'click', function(event) {
                latt=parseFloat(event.latLng.lat());
                lngg=parseFloat(event.latLng.lng());
                if (isCustomPick) {
                    console.log(latt);
                    console.log(lngg);
                    placeMarker(event.latLng);
                }
            });

        }
        
        google.maps.event.addDomListener(window, 'load', initialize);

         $(document).on('click', '#cPick', function(){
            isCustomPick = !isCustomPick;
            
            if(isCustomPick){
                map.setOptions({draggableCursor:'crosshair'});
            }
            else{
                map.setOptions({draggableCursor:'grab'});
            }
            console.log(isCustomPick);
        });
        
        function setCenter(lat, lng){
            map.setCenter(new google.maps.LatLng(lat, lng));
        }

        function setZoom(zoom){
            map.setZoom(zoom);
        }

        function placeMarker(location) {
            var image = image = "{{asset('images/pin/pin_tempat-wisata_24x32.png')}}";
            var marker = new google.maps.Marker({
                position: location,
                map: map,
                icon : image
            });
            var infowindow = new google.maps.InfoWindow({
                content: '<form><fieldset><legend>Informasi:</legend>Nama Tempat:<br><input type="text" name="firstname" placeholder="Tempat Saya"><br><br><input type="submit" value="Simpan"></fieldset></form>'
            });
            infowindow.open(map,marker);
        }

    </script>

    <script>
        $(document).ready(function(){
            //load planner
            //localStorage.clear();
            @if(!is_null($tempat_json))
            localStorage['planner'] = '{!! $tempat_json !!}';
            @endif
            //console.log(localStorage['planner']);
            if(typeof localStorage['planner'] !== "undefined") {
                renderPlanner();
            }
            //load wisata
            $.ajax({
                url: "{{url('sidebar/wisata/load/'.$wilayah->id)}}",
                cache: false
            })
                    .done(function( html ) {
                        $('#tab_wisata > .dest').html(html)
                        $( "#results" ).append( html );
                    });
            $.ajax({
                url: "{{url('sidebar/penginapan/load/'.$wilayah->id)}}",
                cache: false
            })
                    .done(function( html ) {
                        $('#tab_penginapan > .dest').html(html)
                        $( "#results" ).append( html );
                    });
            $.ajax({
                url: "{{url('sidebar/oleh/load/'.$wilayah->id)}}",
                cache: false
            })
                    .done(function( html ) {
                        $('#tab_oleh > .dest').html(html)
                        $( "#results" ).append( html );
                    });

            $(document).on('click', '.btn-planner-save', function(){
                var name = $('.input-planner-name').val();
                var planner = localStorage['planner'];
                //console.log(JSON.parse(planner));
                $.ajax({
                    method: "POST",
                    url: "{{url('planner/save')}}",

                    dataType: 'json',
                    data: { 'name': name, 'planner': planner }
                })
                        .done(function( html ) {
                            $('.modal').modal('hide');
                            $('.success-save').modal('show');
                        });
            });


            $(document).on('click', '.btn-plan-analisis', function(){
                $('#general_modal_lg').html($('#loading_modal_lg').html());

                $('.modal').modal('hide');
                $('.general-modal-lg').modal('show');
                // alert(instructions);
                $.ajax({
                    method: "POST",
                    url: "{{url('planner/analysis')}}",
                    dataType: 'html',
                    data: {'instructions' : instructions}
                })
                        .done(function( html ) {
                            // $('.general-modal-lg').modal('hide');
                            // $('.modal').modal('hide');
                            $('#general_modal_lg').html(html);

                            // $('.general-modal-lg').modal('show');
                        });
            });

            $.ajax({
                url: "{{url('sidebar/transit/load/')}}",
                cache: false
            })
                    .done(function( html ) {

                        $('#tab_transit').html(html);
                        $( "#results" ).append( html );

                        $('.toggle-btn-left').css('left',$('.left-sidebar').width());
                        $('.toggle-btn-left').next().addClass('active');
                        $('.toggle-btn-left').addClass('active');
                        $('.toggle-btn-right').css('right',$('.right-sidebar').width());
                        $('.toggle-btn-right').next().addClass('active');
                        $('.toggle-btn-right').addClass('active');
                    });

            $(document).on('click', '.btn-planner-add', function(){
                var idTempat = $(this).attr('data-id');
                var tempat = $(this).parent().parent().parent();
                console.log($(this).parent().parent().children("td.jumlah").children().val())

                addToPlanner(tempat, idTempat);


            });

            $(document).on('click', '.btn-position-change',function(e){
                e.preventDefault();
                var planner = JSON.parse(localStorage['planner']);
                var handler = $(this).attr('data-handler');
                var id = $(this).attr('data-id');
                var position = searchPositionByID(id, planner);
                if(handler == "up"){
                    up(position, planner);
                } else {
                    down(position, planner);
                }
                var stringifyJson = JSON.stringify(planner);
                //console.log(stringifyJson);
                localStorage['planner'] = stringifyJson;
                renderPlanner();
                createDirection();
            });


            $(document).on('click', '.btn-delete-item-planner', function(e){
                e.preventDefault();
                var id = $(this).attr('data-id');
                //console.log(localStorage['planner'].length);
                deleteItemPlanner(id);
                renderPlanner();
                //console.log(localStorage['planner'].length);
            });

            $(document).on('click', '.btn-transit-add', function(e){
                //console.log($(this).attr('data-id'));
                urls = "{{url('tempat/transitByCode')}}"+"/"+$(this).attr('data-id');
                var harga_total = $(this).attr('data-harga');
                //console.log(harga_total);
                $.ajax({
                    url: urls,
                    cache: false
                })
                        .done(function( html ) {
                            html.harga_tiket_masuk = harga_total;
                            //console.log(html);
                            addToPlanner(html, html['id']);
                        });
            });
            function showModalAlert(title, text, timeout){
                $('.modal').modal('hide');
                $('.alert-modal-title').html(title);
                $('.alert-modal-body').html(text);
                $('.alert-modal').modal('show');
                setTimeout(function() { $('.alert-modal').modal('hide'); }, timeout);
                return;
            }

            function addToPlanner(tempat, idTempat){
                if(localStorage.length !== 0){
                    var planner = JSON.parse(localStorage['planner']);
                    //$(tempat).remove();
                    if(searchPositionByID(idTempat, planner) >= 0){
                        showModalAlert("Opps!", "Kamu sudah menambahkan tempat ini ke rencana kamu!", 2000);
                        return;
                    }

                }

                $.ajax({
                    url: "{{url('planner/tempat/add/')}}"+"/"+idTempat,
                    cache: false
                })
                        .done(function( html ) {
                            var json = $(html).find('.json-result').val();
                            json = JSON.parse(json);
                            //console.log(json);
                            //console.log(json.flag);
                            if(json.flag == 3){
                                //console.log("masyuk");
                                json.harga_tiket_masuk = tempat.harga_tiket_masuk;
                                //console.log(json);
                            }
                            if(localStorage.length === 0){
                                var planner = [json];
                                localStorage["planner"] = JSON.stringify(planner);
                            } else {
                                var planner = JSON.parse(localStorage['planner']);
                                planner.push(json);
                                localStorage["planner"] = JSON.stringify(planner);
                            }
                            //
                            renderPlanner();
                            setVisibilitySavePlan();
                            createDirection();
                            showModalAlert("Selamat!", json.nama+" berhasil ditambahkan ke rencana wisatamu!", 2000);
                        });
            }

            function up(index, list){
                if(index == 0)
                    return
                swap(index, index-1, list);
                return
            }

            function down(index, list){
                if(index == list.length-1)
                    return;
                swap(index, index+1, list);
                return;
            }

            function swap(i, j, list){
                var temp = list[i];
                list[i] = list[j];
                list[j] = temp;
                return;
            }
            function parseRecursive(list){
                var arrParse = [];
                JSON.parse(list).forEach(function(item){
                    arrParse.push(JSON.parse(item));
                });
                return arrParse;
            }

            function stringifyRecursive(list){
                var arrStrngify = [];
                list.forEach(function(item){
                    arrStrngify.push(JSON.stringify(item));
                });
                arrStrngify = JSON.stringify(arrStrngify);
                return arrStrngify;
            }

            function searchPositionByID(id, list){
                var position = -1;
                var counter = 0;
                list.forEach(function(item){
                    if(parseInt(item.id) == parseInt(id)) {
                        position = counter
                        return;
                    }
                    counter++;
                });
                return position;
            }

            function renderPlanner(){
                $('.planner-result').html('');

                var json_planner = JSON.parse(localStorage['planner']);


                console.log(json_planner);
                if (json_planner.length > 0) {
                    json_planner.forEach(function (item) {
                        //item = JSON.parse(item);
                        setMarkerIcon(item.id, item.flag, true);
                        var tempat = '<li class="dest-list col-md-12"><div class="col-md-1"><a class="btn-position-change no-padding col-md-12" data-handler="up" data-id="'+item.id+'" href="#"><i class="fa fa-angle-up fa-2x"></i></a><a data-id="'+item.id+'" href="#" data-handler="down" class="no-padding btn-position-change col-md-12"><i class="fa fa-2x fa-angle-down"></i></a></div><div class="col-md-4"><div class="tab-img2"><img src="{{asset("")}}'+item.image_url+'"></div></div><div class="col-md-3"><p class="plan-title">' + item.nama + '</p><div class="route" id="route '+item.id+'"><p class="jarak"><i class="fa fa-street-view"></i>'+0+'</p><p class="waktu"><i class="fa fa-clock-o"></i> 0min</p></div></div><div class="col-md-3"><p class="plan-price text-center">Rp' + item.harga_tiket_masuk + '</p></div><div class="col-md-1"><div class="plan-delete"><a href="#" class="btn-delete-item-planner" data-id="'+item.id+'"><i class="fa fa-trash-o fa-2x"></i></a></div></div></li>';
                        
                        $('.planner-result').append(tempat);
                    });
                }
            }

            function deleteItemPlanner(id){
                var planner = JSON.parse(localStorage['planner']);
                var position = searchPositionByID(id, planner);
                var newArray = planner.splice(position, 1); 
                setMarkerIcon(newArray[0]['id'],newArray[0]['flag'], false);
                localStorage['planner'] = JSON.stringify(planner);
                setVisibilitySavePlan();
                createDirection();
            }
        });

        function setFlights(adult, child, infant){
            var departure = $('#p_departure').val();
            var arrival = $('#p_arrival').val();
            var date = $('#p_date').val();
            urls = "{{url('transportasi/pesawat')}}"+"/"+departure+"/"+arrival+"/"+date+"/"+adult+"/"+child+"/"+infant;
            //console.log();
            $('#tab_transportasi').html("Loading . . .");
            $.ajax({
                url: urls,
                cache: false
            })
                    .done(function( html ) {
                        $('#tab_transportasi').html(html)
                        $( "#results" ).append( html );
                    });
        }

        function setTrains(adult, child){
            var departure = $('#t_departure').val();
            var arrival = $('#t_arrival').val();
            var date=  $('#t_date').val();
            console.log(date);

            
            urls = "{{url('transportasi/kereta')}}"+"/"+departure+"/"+arrival+"/"+date+"/"+adult+"/"+child;
            //console.log(urls);
            $('#tab_transportasi').html("Loading . . .");
            $.ajax({
                url: urls,
                cache: false
            })
                    .done(function( html ) {
                        $('#tab_transportasi').html(html)
                        $( "#results" ).append( html );
                    });
        }

        function setModalTempat(urls){
            $.ajax({
                url: urls,
                cache: false
            })
                    .done(function( html ) {
                        $('#detail_tempat').html(html)
                        $( "#results" ).append( html );
                        $('#modal_tempat').modal('show');
                    });       
        }
       
        function setVisibilitySavePlan(){
            if(typeof localStorage['planner'] !== "undefined"){
                if (localStorage['planner'].length > 2 ) {
                    $("#save_plan").removeClass("hidden");
                } else {
                    $("#save_plan").addClass("hidden");
                }

            } else {
                $("#save_plan").addClass("hidden");
            }
        }
        function createDirection(){
            if(typeof localStorage['planner'] !== "undefined"){
                if (localStorage['planner'].length > 2) {
                    var places = JSON.parse(localStorage['planner']);
                    if (places.length > 1) {
                        //console.log(localStorage['planner']);
                        makeDirection(places);
                    } else {
                        removeDirection();
                    }
                } else {
                    removeDirection();
                }
            } else {
                removeDirection();
            }
        }
        setVisibilitySavePlan();
    </script>


    <script>
        $(document).on('click','.btn-view-on-map', function(){
            var lat = $(this).attr('data-lat');
            var lng = $(this).attr('data-lng');

            map.panTo(new google.maps.LatLng(lat, lng));
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('body').height($(window).height());
            //console.log($(window).height()-($('.header--wilayah').height()));
            $('#map_canvas').height($(window).height()-($('.header--wilayah').height()));
        });
    </script>

    <script>
        var timer;

        $(".input-search").on('keyup', function() {
            var qSearch = $(this).val();
            clearInterval(timer);  //clear any interval on key up
            timer = setTimeout(function(e) { //then give it a second to see if the user is finished
                //do .post ajax request //then do the ajax call
                    $.ajax({
                        url: "{{url('search/inner/')}}/"+qSearch,
                    })
                            .done(function( html ) {
                                $('.sidebar-content').html(html);
                                deleteMarkers();
                                var markers_search = JSON.parse($('#markers').html());
                                if(markers_search.length > 0){
                                    setCenter(parseFloat(markers_search[0].lat), parseFloat(markers_search[0].lng));
                                    setZoom(13);
                                }
                                addMarkers(markers_search);
                            });
                
            }, 1000);
        });

    </script>

@endsection