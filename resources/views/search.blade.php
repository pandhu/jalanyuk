<div style="padding:10px;">
    @if(!count($wilayah) == 0)
        <span style="font-weight: bold">Wilayah</span>
        @foreach ($wilayah as $item)
            <li>
                <a href="{{url('wilayah/'.$item->id)}}">
                    {{$item->nama}}
                </a>
            </li>
        @endforeach
    @endif
    <div class="clearfix"></div>
    @if(!count($wisata) == 0)
        <span style="font-weight: bold">Tempat Wisata</span>
        @foreach ($wisata as $item)
            <li>
                <a href="{{url('wisata/'.$item->id)}}">
                    {{$item->nama}}
                </a>
            </li>
        @endforeach
    @endif
    @if(count($wilayah) ==0 && count($wisata)==0)
        <span class="text-danger"><i>Tidak Ditemukan</i></span>
    @endif
</div>