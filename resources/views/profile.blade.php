@extends('layout.master')

@section('master-content')
<section class="profile-content">
	<div class="container">
		<div class="row">
			<div class="panel panel-default panel-custom2">
				<div class="panel-heading panel-head-custom">
					<div class="row">
						<div class="col-md-12 text-center">
							<h4>Ubah Profil</h4>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
						<form class="form-horizontal">
				            <div class="col-md-12">
				                <div class="form-group">
				                	<div class="col-md-3"></div>
				                    <label class="col-md-2 control-label">Username</label>
				                    <div class="col-md-3">
				                        <input class="form-control form-custom" placeholder="username" type="text" value="{{Auth::user()->username}}">
				                    </div>
				                    <div class="col-md-3"></div>
				                </div>
				                <div class="form-group">
				                	<div class="col-md-3"></div>
				                    <label class="col-md-2 control-label">Email</label>
				                    <div class="col-md-3">
				                        <input class="form-control form-custom" placeholder="example@blabla.com" type="text" value="{{Auth::user()->email}}">
				                    </div>
				                    <div class="col-md-3"></div>
				                </div>
				                <div class="form-group">
				                	<div class="col-md-3"></div>
				                    <label class="col-md-2 control-label">Password Lama</label>
				                    <div class="col-md-3">
				                        <input class="form-control form-custom" type="text">
				                    </div>
				                    <div class="col-md-3"></div>
				                </div>
				                <div class="form-group">
				                	<div class="col-md-3"></div>
				                    <label class="col-md-2 control-label">Password Baru</label>
				                    <div class="col-md-3">
				                        <input class="form-control form-custom" type="text">
				                    </div>
				                    <div class="col-md-3"></div>
				                </div>
				                <div class="form-group">
				                	<div class="col-md-3"></div>
				                    <label class="col-md-2 control-label">Konfirmasi Password</label>
				                    <div class="col-md-3">
				                        <input class="form-control form-custom" type="text">
				                    </div>
				                    <div class="col-md-3"></div>
				                </div>
				                <div class="form-group">
				                	<div class="col-md-3"></div>
				                    <div class="col-md-2"></div>
				                    <div class="col-md-3">
				                        
				                        <button class="btn btn-default btn-admin pull-right" type="submit">Simpan</button>
				                    </div>
				                </div>
				            </div>
				        </form>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
</section>


@endsection
@section('master-js')
@endsection