<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Jalan Yuk!</title>

    <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/main.css')}}" rel="stylesheet">
    <link rel="icon" href="{{ asset('/images/logo.png') }}" type="image/gif" sizes="16x16">
</head>
<body>
    <header class="header--wilayah">
        <div class="header__logo">
            <a href="{{ url('/') }}"><img src="{{asset('/images/logo2.png')}}" width="120px" height="60px"></a>
        </div>
    </header>
    <section class="form-content">
        <form class="form-horizontal">
            <div class="col-md-12 form-admin">
                <div class="form-group">
                    <label class="col-md-2 control-label">Nama</label>
                    <div class="col-md-3">
                        <input class="form-control form-custom" placeholder="Nama wilayah" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Koordinat</label>
                    <div class="col-md-6">
                        <input class="form-control form-custom" id="lat" placeholder="Lat" type="text">
                        <input class="form-control form-custom" id="lng" placeholder="Lng" type="text">
                        <div id="map-canvas"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Area</label>
                    <div class="col-md-6">
                        <input class="form-control form-custom" id="lat" placeholder="Lat" type="text">
                        <input class="form-control form-custom" id="lng" placeholder="Lng" type="text">
                        <div id="map-canvas"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Parent</label>
                    <div class="col-md-3">
                        <select id="subject" name="subject" class="form-control form-custom">
                            <option selected value="na">
                                Pilih Satu:
                            </option>
            
                            <option>
                                Eksekutif
                            </option>
            
                            <option>
                                Bisnis
                            </option>
            
                            <option>
                                Ekonomi
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <button class="btn btn-default btn-admin"><a href="{{ url('admin') }}">Back to Admin Page</a></button>
                        <button class="btn btn-default btn-admin" type="submit">Send</button>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCywKil0QHZtfNmEwvm-tCLucheDOuXGKA&libraries=places"></script>
    <script src="{{asset('js/admin.js')}}"></script>
    <script src="{{asset('/js/jquery-1.11.3.js')}}"></script>
    <script src="{{asset('/js/bootstrap.js')}}"></script>
</body>
</html>