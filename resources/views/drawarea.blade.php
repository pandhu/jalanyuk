@extends('layout.master')
@section('master-css')
    
@endsection
@section('master-content')
    <section class="col-md-12 no-padding map-content">
        <div id="left_bar" class="tempat-wisata col-md-4 no-padding">

            <div class="tabbable-panel">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_default_1" data-toggle="tab">
                            <i class="fa fa-bicycle"></i> Atur Maps </a>
                        </li>
                        <li>
                            <a href="#tab_default_2" data-toggle="tab">
                            <i class="fa fa-building"></i> Zonk </a>
                        </li>
                        <li>
                            <a href="#tab_default_3" data-toggle="tab">
                            <i class="fa fa-shopping-cart"></i> Zonk </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_default_1">
                            <ul class="dest">
                                <li class="dest-list col-md-12">        
                                    <div class="col-md-5">
                                        <div class="tab-img">
                                            Select Input
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <form>
                                            <input type="radio" name="mode" value=0 onclick="changeState(this.value)" checked>Draw Your Imagination
                                            <br>
                                            <input type="radio" name="mode" value=1 onclick="changeState(this.value)">Draw Area
                                            <br>
                                            <input type="radio" name="mode" value=2 onclick="changeState(this.value)">Draw Marker
                                            <br>
                                            <input type="radio" name="mode" value=3 onclick="changeState(this.value)">Direction
                                        </form>
                                    </div>
                                </li>
                                <li class="dest-list col-md-12">        
                                    <div class="col-md-5">
                                        <div class="tab-img">
                                            Drawing Area
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <button  type="button" class="btn btn-primary" onclick="drawArea()">Draw Area</button>
                                        <button  type="button" class="btn btn-danger" onclick="removeArea()">Remove Area</button>
                                        <div class="dest-star">
                                            Koordinat :
                                           <div id="koordinat"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="dest-list col-md-12">        
                                    <div class="col-md-5">
                                        <div class="tab-img">
                                            Marker
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="dest-star">
                                            Marker :
                                           <div id="marker"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="dest-list col-md-12">        
                                    <div class="col-md-5">
                                        <div class="tab-img">
                                            Direction
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <button  type="button" class="btn btn-primary" onclick="makeDirection()">Direction</button>
                                        <div class="dest-star">
                                            Route :
                                           <div id="route"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="dest-list col-md-12">        
                                    <div class="col-md-5">
                                        <div class="tab-img">
                                            GeoCode
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="dest-star">
                                           <div id="directions-panel">
                                                <input id="geocoding_address"type="text" >
                                                <button type="button" class="btn btn-primary btn-geocoding">GeoCoding!</button>
                                                <div id="geocoding_result"></div>
                                           </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane" id="tab_default_2">
                            <ul class="dest">
                                <li class="dest-list col-md-12">        
                                    <div class="col-md-5">
                                        <div class="tab-img">
                                            <img src="{{asset('/images/wisata/Pantai-Parangtritis.jpg')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <a href="#" class="dest-title">Pantai Parangtritis</a>
                                        <div class="dest-star">
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane" id="tab_default_3">
                            <ul class="dest">
                                <li class="dest-list col-md-12">        
                                    <div class="col-md-5">
                                        <div class="tab-img">
                                            <img src="{{asset('/images/wisata/Pantai-Parangtritis.jpg')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <a href="#" class="dest-title">Pantai Parangtritis</a>
                                        <div class="dest-star">
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
          <div id="koordinat"></div>
        </div>

        <div class="map-content col-md-8 no-padding">
            <div id="map_canvas"></div>
        </div>
    </section>
@endsection
@section('master-js')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCywKil0QHZtfNmEwvm-tCLucheDOuXGKA&libraries=places"></script>
    <!-- <script src="{{asset('js/script.js')}}"></script>
    -->
    <script src="{{asset('js/drawOverlay.js')}}"></script>
    <script>
        $(document).ready(function(){
            $(document).on('click', '.btn-geocoding', function(){
                // var name = $('.input-planner-name').val();
                var address = $('#geocoding_address').val();
                geoCoding(address);
                //console.log(JSON.parse(planner));
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('body').height($(window).height());
            console.log($(window).height()-($('.header--wilayah').height()));
            $('#map_canvas').height($(window).height()-($('.header--wilayah').height()));
        });
    </script>

    
@endsection