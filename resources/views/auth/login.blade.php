<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Jalan Yuk!</title>

    <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/main.css')}}" rel="stylesheet">
    <link rel="icon" href="{{ asset('/images/logo.png') }}" type="image/gif" sizes="16x16">
</head>
<body>
	<!-- Header Login & Sign Up Page -->
	<header class="header--wilayah">
		<div class="header__logo">
	        <a href="{{ url('/') }}"><img src="{{asset('/images/logo2.png')}}" width="120px" height="60px"></a>
	    </div>
	</header>
	<section class="login">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default panel-custom">
						<div class="panel-heading panel-head-custom">
							<div class="row">
								<div class="col-xs-6 text-center">
									<a href="#" class="active" id="login-form-link">Masuk</a>
								</div>
								<div class="col-xs-6 text-center">
									<a href="#" id="register-form-link">Daftar</a>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									@if (count($errors) > 0)
										<div class="alert alert-danger">
											<strong>Whoops!</strong> There were some problems with your input.<br><br>
											<ul>
												@foreach ($errors->all() as $error)
													<li>{{ $error }}</li>
												@endforeach
											</ul>
										</div>
									@endif
								
									<form id="login-form" class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										<div class="input-group" style="margin-bottom: 15px">
											<span class="input-group-addon addon-custom"><i class="glyphicon glyphicon-user"></i></span>
											<input type="text" class="form-control form-custom" name="username" value="{{ old('username') }}" placeholder="Username">
											
										</div>

										<div class="input-group" style="margin-bottom: 15px">
											<span class="input-group-addon addon-custom"><i class="glyphicon glyphicon-lock"></i></span>
											<input type="password" class="form-control form-custom" name="password" placeholder="Password">
											
										</div>

										<div class="input-group" style="margin-bottom: 15px">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="remember"> Ingat Saya
												</label>
											</div>
										</div>

										<div class="input-group">
											<button type="submit" class="btn btn-primary btn-login">Masuk</button>

											<a class="btn btn-link link-custom" href="{{ url('/password/email') }}">Lupa Password?</a>
										</div>
									</form>
									<form id="register-form" action="{{url('auth/register')}}" method="post" role="form" style="display: none;">
										<div class="input-group" style="margin-bottom: 15px">
											<span class="input-group-addon addon-custom"><i class="glyphicon glyphicon-user"></i></span>
											<input type="text" name="username" id="username" tabindex="1" class="form-control form-custom" placeholder="Username" value="">
										</div>
										<div class="input-group" style="margin-bottom: 15px">
											<span class="input-group-addon addon-custom"><i class="glyphicon glyphicon-envelope"></i></span>
											<input type="email" name="email" id="email" tabindex="1" class="form-control form-custom" placeholder="Email Address" value="">
										</div>
										<div class="input-group" style="margin-bottom: 15px">
											<span class="input-group-addon addon-custom"><i class="glyphicon glyphicon-lock"></i></span>
											<input type="password" name="password" id="password" tabindex="2" class="form-control form-custom" placeholder="Password">
										</div>
										<div class="input-group" style="margin-bottom: 15px">
											<span class="input-group-addon addon-custom"><i class="glyphicon glyphicon-ok"></i></span>
											<input type="password" name="password_confirmation" id="confirm-password" tabindex="2" class="form-control form-custom" placeholder="Konfirmasi Password">
										</div>
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
										<div class="form-group">
											<div class="row">
												<div class="col-sm-6 col-sm-offset-3">
													<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Daftar Sekarang">
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	
	<script src="{{asset('/js/jquery-1.11.3.js')}}"></script>
	<script src="{{asset('/js/bootstrap.js')}}"></script>
	<script src="{{asset('/js/loginTab.js')}}"></script>
</body>
</html>