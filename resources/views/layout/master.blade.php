<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!! csrf_token() !!}"/>

    <title>Jalan Yuk!</title>

    <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/main.css')}}" rel="stylesheet">
    <link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
    <link rel="icon" href="{{ asset('/images/logo.png') }}" type="image/gif" sizes="16x16">

    @yield('master-css')
</head>
<body>
<header class="header--wilayah ">
    <div class="row no-padding no-margin">
        <div class="content__search-input col-md-5">
            <div class="input-group input-wilayah">
                <form class="search-default" action="{{url('show')}}" method="GET">
                    <input type="text" id="search-navbar" class="form-control input-md input-md-wilayah dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" placeholder="Co: Yogya atau Borobudur">
                    <input type="hidden" name="id" id="search-navbar-id">
                    <input type="hidden" name="type" id="search-navbar-type">
                    <span class="input-group-btn input-search">
                        <button type="submit" class="btn-info btn-custom2 btn-md">
                            <i class="glyphicon glyphicon-search glyphicon-search-wilayah"></i>
                        </button>
                        <button type="button" class="btn btn-default" id="cPick">
                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                        </button>
                    </span>
                </form>

            </div>
        </div>
        <div class="header__logo col-md-2">
            <a href="{{ url('/') }}"><img src="{{asset('/images/logo2.png')}}" width="120px" height="60px"></a>
        </div>
        <div class="col-md-5">
            <ul class="nav navbar-nav nav-user">
                @if(Auth::check())
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{Auth::user()->username}}<span class="glyphicon glyphicon-user pull-right"></span></a>

                    <ul class="dropdown-menu menu-user">
                        <li>
                            <a href="{{ url('profile') }}">Ubah profil <span class="glyphicon glyphicon-cog pull-right"></span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url('user-plan') }}">Lihat Rencana <span class="glyphicon glyphicon-heart pull-right"></span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{url('auth/logout')}}">Keluar <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                        </li>
                    </ul>
                </li>
                @else
                    <a href="{{url('auth/login')}}"><button class="dropdown dropdown-user">Login</button></a>
                @endif  
            </ul>
        </div>
    </div>
</header>

@yield('master-content')
<!--
<footer class="site-footer">
        <p>Copyright &copy; Tafakur Alam 2015</p>
    </footer>
-->
<script src="{{asset('js/jquery-1.11.3.js')}}"></script>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/sidebar.js')}}"></script>
<script src="{{asset('js/transportTab.js')}}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
</script>
<script>
            $(function() {
                $( "#search-navbar" ).autocomplete({
                    minLength: 0,
                    source: "{{url('/')}}/search"+$(this).val(),
                    focus: function( event, ui ) {
                        $( "#search-navbar" ).val( ui.item.label );
                        return false;
                    },
                    select: function( event, ui ) {
                        $( "#search-navbar" ).val( ui.item.nama );
                        $( "#search-navbar-id").val( ui.item.id );
                        $( "#search-navbar-type").val( ui.item.type);
                        $( "#search-navbar-description" ).html( ui.item.nama );
                        return false;
                    }
                })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {
                    return $( "<li>" )
                    .append( "<a href='{{url("show")}}?"+"id="+item.id+"&type="+item.type+"'>" + item.nama + " <sub>"+item.type +"</sub>"+ "<br>"+ "</a>" )
                    .appendTo( ul );
                };
            });
</script>

@yield('master-js')
</body>
</html>
