@extends('layout.master')
@section('master-content')
<div class="margin-70"></div>
<section class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="content__site-quote text-center">"Rencanakan Wisatamu!"</h1>
				<h5 class="text-center">
					@if (Auth::check())
					Hallo {{Auth::user()->username}} !
					@else
					Ingin masuk ke akun Anda? Atau belum mempunyai akun? Klik <a href="{{ url('auth/login') }}">disini</a></h5>
					@endif
					<br>
					<br>

					<div class="col-md-4"></div>
					<div class="col-md-4">

						<h4 class="content__site-label">Masukkan wilayah atau nama tempat wisata</h6>

							<div class="content__search-input">
								<div class="input-group input-home">
									<form class="search-home" action="{{url('show')}}" method="GET">
										<input type="text" id="search" class="form-control input-lg input-lg-home dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" placeholder="Co: Sleman atau Borobudur">
										<input type="hidden" name="id" id="search-id">
										<input type="hidden" name="type" id="search-type">
										<span class="input-group-btn input-search">
											<button type="submit" class="btn-info btn-custom1 btn-lg">
												<i class="glyphicon glyphicon-search glyphicon-search-home"></i>
											</button>
										</span>
									</form>
								</div>
							</div>
							


						</div>
						<div class="col-md-4"></div>
					</div>
				</div>
			</div>
		</section>
		<!-- </div> -->


	</head>
	<body>

		@endsection

		@section('master-js')
		<script>
			$(function() {
		        $( "#search" ).autocomplete({
		        	minLength: 0,
		        	source: "{{url('/')}}/search"+$(this).val(),
		        	focus: function( event, ui ) {
		        		$( "#search" ).val( ui.item.label );
		        		return false;
		        	},
		        	select: function( event, ui ) {
		        		$( "#search" ).val( ui.item.nama );
		        		$( "#search-id").val( ui.item.id );
		        		$( "#search-type").val( ui.item.type);
		        		$( "#search-description" ).html( ui.item.nama );
		        		$( "#search-icon" ).attr( "src", "images/" + ui.item.icon );

		        		return false;
		        	}
		        })
		        .autocomplete( "instance" )._renderItem = function( ul, item ) {
		        	return $( "<li>" )
		        	.append( "<a href='{{url("show")}}?"+"id="+item.id+"&type="+item.type+"'>" + item.nama + " <sub>"+item.type +"</sub>"+ "<br>"+ "</a>" )
		        	.appendTo( ul );
		        };
		    });
</script>


<script>

</script>
@endsection
</body>
</html>