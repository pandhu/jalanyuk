@foreach($tempat as $item)
    <li class="dest-list col-md-12">
        <div class="col-md-5">
            <div class="tab-img">
                <img src="{{asset($item->gallery[0]->foto)}}">
            </div>
        </div>
        <div class="col-md-7">
            <a href="#" class="dest-title"  onclick="setModalTempat('{{url('detail/wisata/'.$item->flag.'/'.$item->id)}}')">{{$item->nama}}</a>
            <div class="dest-star">
                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
            </div>
            <row>
                <a data-lat="{{$item->lat}}" data-lng="{{$item->lng}}" class="btn btn-xs btn-default btn-view-on-map" data-toggle="tooltip" data-placement="bottom" title="Lihat dalam Peta">
                    <i class="fa fa-search-plus"></i>
                </a>
                <a data-id="{{$item->id}}" data-link="{{url('planner/add/'.$item->id)}}" class="btn btn-xs btn-warning btn-planner-add" data-toggle="tooltip" data-placement="bottom" title="Tambah ke Rencana">
                    <i class="fa fa-plus"></i>
                </a>
            </row>
        </div>

    </li>
@endforeach