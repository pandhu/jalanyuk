
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-custom">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Analisa Rencana</h3>
            </div>
            
            <div class="modal-body">
                
                <div class="detail-tab">
                    <div class="col-md-12">
                        <div class="tabbable-panel">
                            <div class="tabbable-line">
                                <ul class="nav nav-tabs ">
                                    <li class="active">
                                        <a href="#deskripsi" data-toggle="tab">
                                        <i class="fa fa-book"></i> Analisis </a>
                                    </li>
                                    <li>
                                        <a href="#keunikan" data-toggle="tab">
                                        <i class="fa fa-binoculars"></i> Penunjuk Arah </a>
                                    </li>
                                    <li>
                                        <a href="#wahana" data-toggle="tab">
                                        <i class="fa fa-paw"></i> Catatan </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade in active" id="deskripsi">
                                <div class="col-md-12" style="text-align: justify">
                                    aaaaaaaa
                                </div>
                            </div>
                            <div class="tab-pane fade" id="keunikan">
                                <div class="col-md-12" style="text-align: justify">
                                    bbbbbb
                                    {{$instructions}}
                                </div>                           
                            </div>
                            <div class="tab-pane fade" id="wahana">
                                <div class="col-md-12" style="text-align: justify">
                                    ccccccc
                                </div>                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer no-border">
                
            </div>
        </div>
    </div>
