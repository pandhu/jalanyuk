<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Pergi</th>
                <th>Tiba</th>
                <th>Durasi</th>
                <th>Harga</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        	@foreach ($flights->departures->result as $flight)
                <tr>
                    <td>{{$flight->airlines_name}}</td>
                    <td>{{$flight->simple_departure_time}}</td>
                    <td>{{$flight->simple_arrival_time}}</td>
                    <td>{{$flight->duration}}</td>
                    <td>Rp {{$flight->price_value}}</td>
                    <td>
                        <button data-id="{{$flights->search_queries->to}}" data-harga="{{$flight->price_value}}" class="btn btn-default btn-pilih btn-transit-add" >Pilih</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

