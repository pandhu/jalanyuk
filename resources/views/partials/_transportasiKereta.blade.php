<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Pergi</th>
                <th>Tiba</th>
                <th>Durasi</th>
                <th>Kelas</th>
                <th>Harga</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        	@foreach ($trains->departures->result as $train)
                <tr>
                    <td>{{$train->train_name}}</td>
                    <td>{{$train->departure_time}}</td>
                    <td>{{$train->arrival_time}}</td>
                    <td>{{$train->duration}}</td>
                    <td>{{$train->class_name_lang}}</td>
                    <td>{{$train->price_total}}</td>
                    <td>
                        <button data-id="{{$trains->search_queries->arr_station}}" data-harga="{{$train->price_total}}" class="btn btn-default btn-pilih btn-transit-add" >Pilih</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

