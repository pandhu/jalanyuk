@if(!is_null($wisataOnPlanner))
    @foreach($wisataOnPlanner as $item)
        <li class="dest-list col-md-12">
            <div class="col-md-4">
                <div class="tab-img2">
                    <img src="{{asset('/images/wisata/Pantai-Parangtritis.jpg')}}">
                </div>
            </div>
            <div class="col-md-4">
                <p class="plan-title">{{$item->nama}}</p>
                <div class="route">
                    <p class="jarak"><i class="fa fa-street-view"></i> Start</p>
                    <p class="waktu"><i class="fa fa-clock-o"></i> 0min</p>
                </div>
            </div>
            <div class="col-md-3">
                <p class="plan-price text-center">Rp 0</p>
            </div>
            <div class="col-md-1">
                <div class="plan-delete">
                    <a href="#"><i class="fa fa-trash-o fa-2x"></i></a>
                </div>

            </div>
        </li>
    @endforeach
@endif