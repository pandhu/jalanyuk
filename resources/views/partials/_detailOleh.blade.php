<div class="modal fade detail-oleh" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_tempat">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-custom">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">{{$tempat->nama}}</h3>
            </div>
            <?php 
                $galleries = $tempat->gallery;
            ?>
            <div class="modal-body">
                <div class="description">
                    <div class="col-md-8">
                        <div class="main-photo">
                            <img src="{{asset($gallery[0]->foto)}}">
                        </div>
                        <div class="gallery">  
                            <div class="carousel slide media-carousel" id="media">
                                <div class="carousel-inner">
                                    <div class="#">
                                        <div class="#">
                                            @foreach ($galleries as $gallery)
                                                <div class="col-md-4">
                                                    <a class="thumbnail" href="#">
                                                        <img src="{{asset('').$gallery->foto}}">
                                                    </a>
                                                </div>          
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="description-item">
                            <h5 class ="modal-title" style="text-transform: uppercase"><i class="fa fa-envelope-o"></i> Alamat</h5>
                            <h6>{{$tempat->alamat}}</h6>
                        </div>
                        <div class="description-item">
                            <h5 class ="modal-title">Berikan bintangmu disini!</h5>
                            <div class="dest-star">
                                <fieldset class="rating">
                                    <input type="radio" id="star5" name="rating" value="5" />
                                        <label class = "full" for="star5" title="Keren banget!"></label>
                                    <input type="radio" id="star4half" name="rating" value="4 and a half" />
                                        <label class="half" for="star4half" title="Keren!"></label>
                                    <input type="radio" id="star4" name="rating" value="4" />
                                        <label class = "full" for="star4" title="Mantap"></label>
                                    <input type="radio" id="star3half" name="rating" value="3 and a half" />
                                        <label class="half" for="star3half" title="Lumayan mantap"></label>
                                    <input type="radio" id="star3" name="rating" value="3" />
                                        <label class = "full" for="star3" title="Cukup"></label>
                                    <input type="radio" id="star2half" name="rating" value="2 and a half" />
                                        <label class="half" for="star2half" title="Agak kurang"></label>
                                    <input type="radio" id="star2" name="rating" value="2" />
                                        <label class = "full" for="star2" title="Kurang"></label>
                                    <input type="radio" id="star1half" name="rating" value="1 and a half" />
                                        <label class="half" for="star1half" title="Kurang banget"></label>
                                    <input type="radio" id="star1" name="rating" value="1" />
                                        <label class = "full" for="star1" title="Enggak deh"></label>
                                    <input type="radio" id="starhalf" name="rating" value="half" />
                                        <label class="half" for="starhalf" title="Enggak banget"></label>
                                </fieldset>
                            </div>
                        </div>
                        <div class="description-item">
                            <button data-id="{{$tempat->id}}" class="btn btn-default btn-pilih btn-planner-add">Pilih</button>
                        </div>
                    </div>
                </div>
                <div class="detail-tab">
                    <div class="col-md-12">
                        <div class="tabbable-panel">
                            <div class="tabbable-line">
                                <ul class="nav nav-tabs ">
                                    <li class="active">
                                        <a href="#deskripsi" data-toggle="tab">
                                        <i class="fa fa-book"></i> Deskripsi </a>
                                    </li>
                                    <li>
                                        <a href="#produk" data-toggle="tab">
                                        <i class="fa fa-shopping-cart"></i> Produk </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade in active" id="deskripsi">
                             
                                <div class="col-md-12" style="text-align: justify">
                                    {{$tempat->deskripsi}}
                                </div>
                                                                  
                            </div>
                            <div class="tab-pane fade" id="produk">
                                
                                <div class="col-md-12" style="text-align: justify">
                                    {{$tempat->produk}}
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer no-border">
                
            </div>
        </div>
    </div>
</div>