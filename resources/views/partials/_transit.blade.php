<div class="col-md-2 bhoechie-tab-menu">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center">
            <h4 class="glyphicon glyphicon-plane"></h4><br/>Pesawat
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="glyphicon glyphicon-road"></h4><br/>Kereta
        </a>
    </div>
</div>
<div class="col-md-10 bhoechie-tab">
    <!-- flight section -->
    <div class="bhoechie-tab-content active">
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md-6 no-padding-left">
                    <h5 style="font-weight: 800">Berangkat</h5>
                    <select class="btn-transit" id="p_departure">
                        @foreach($bandara as $item)
                            <option value="{{$item->kode}}">
                                <a href="#">{{$item->nama}}</a>
                            </option>
                        @endforeach
                    </select>
                </div>
                    
                <div class="col-md-6 no-padding-left">
                    <h5 style="font-weight: 800">Tujuan</h5>
                    <select class="btn-transit" id="p_arrival">
                        @foreach($bandara as $item)
                            <option value="{{$item->kode}}">
                                <a href="#">{{$item->nama}}</a>
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group" style="margin-bottom: 0">
                <label for="date" class="col-sm-2 control-label label-custom no-padding-left">
                    <i class="fa fa-calendar fa-2x"></i>
                </label>
                <div class="col-sm-10 no-padding-left">
                    <input type="date" class="form-control calendar-custom" id="p_date" name="date">
                </div>
            </div>
            
            <div class="form-group" style="margin-bottom: 0">
                
                <div class="col-md-5 pull-right no-padding-left">
                    <button class="btn btn-default btn-plan" data-toggle="modal" data-target=".detail-transport" onclick="setFlights('1','0','0')">
                        <i class="fa fa-search"></i> Cari Jadwal
                    </button>
                </div>
                
            </div>
        </div>
    </div>    
    <!-- train section -->
    <div class="bhoechie-tab-content">

        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md-6 no-padding-left">
                    <h5 style="font-weight: 800">Berangkat</h5>
                    <select class="btn-transit" id="t_departure">
                        @foreach($stasiun as $item)
                            <option value="{{$item->kode}}">
                                <a href="#">{{$item->nama}}</a>
                            </option>
                        @endforeach
                    </select>
                </div>
                    
                <div class="col-md-6 no-padding-left">
                    <h5 style="font-weight: 800">Tujuan</h5>
                    <select class="btn-transit" id="t_arrival">
                        @foreach($stasiun as $item)
                            <option value="{{$item->kode}}">
                                <a href="#">{{$item->nama}}</a>
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="form-group" style="margin-bottom: 0">
                <label for="date" class="col-sm-2 control-label label-custom no-padding-left">
                    <i class="fa fa-calendar fa-2x"></i>
                </label>
                <div class="col-sm-10 no-padding-left">
                    <input type="date" class="form-control calendar-custom" id="t_date" name="date">
                </div>
            </div>
            
            <div class="form-group" style="margin-bottom: 0">
                
                <div class="col-md-5 pull-right no-padding-left">
                    <button class="btn btn-default btn-plan" data-toggle="modal" data-target=".detail-transport" onclick="setTrains('1','0')">
                        <i class="fa fa-search"></i> Cari Jadwal
                    </button>
                </div>
                
            </div>
        </div>
    </div>
</div>