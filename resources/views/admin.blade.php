<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Jalan Yuk!</title>

    <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/main.css')}}" rel="stylesheet">
    <link rel="icon" href="{{ asset('/images/logo.png') }}" type="image/gif" sizes="16x16">
</head>
<body>
    <header class="header--wilayah">
        <div class="header__logo">
            <a href="{{ url('/') }}"><img src="{{asset('/images/logo2.png')}}" width="120px" height="60px"></a>
        </div>
    </header>
    <section class="admin-content">
        <div class="container">
            <div class="row">
                <div class="admin-oleholeh">
                    <div class="admin-title">
                        <div class="col-md-6">
                            <h2 class="admin-meta">Wilayah</h2>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('form-wilayah') }}"><button type="button" class="btn btn-default btn-admin pull-right">
                                <i class="fa fa-pencil-square-o"></i>
                                Tambah
                            </button></a>
                        </div>
                    </div>
                
                    <div class="table-wilayah">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th>Nama Wilayah</th>
                                        <th>Wilayah</th>
                                        <th class="text-center">Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>Toko Batik</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="tooltip" data-placement="bottom" title="Lihat">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-wilayah') }}"><button type="button" class="btn btn-default btn-admin-option" data-toggle="tooltip" data-placement="bottom" title="Edit">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="modal" data-target=".delete-confirm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="admin-wisata">
                    <div class="admin-title">
                        <div class="col-md-6">
                            <h2 class="admin-meta">Tempat Wisata</h2>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('form-wisata') }}"><button type="button" class="btn btn-default btn-admin pull-right">
                                <i class="fa fa-pencil-square-o"></i>
                                Tambah
                            </button></a>
                        </div>
                    </div>

                    <div class="table-wisata">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th>Nama Tempat</th>
                                        <th>Wilayah</th>
                                        <th class="text-center">Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>Pantai Parangtritis</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="tooltip" data-placement="bottom" title="Lihat">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-wisata') }}"><button type="button" class="btn btn-default btn-admin-option" data-toggle="tooltip" data-placement="bottom" title="Edit">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="modal" data-target=".delete-confirm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td>Malioboro</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-wisata') }}"><button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td>Alun-alun Yogyakarta</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-wisata') }}"><button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4</td>
                                        <td>Gunung Merapi</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-wisata') }}"><button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="admin-penginapan">
                    <div class="admin-title">
                        <div class="col-md-6">
                            <h2 class="admin-meta">Penginapan</h2>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('form-inap') }}"><button type="button" class="btn btn-default btn-admin pull-right">
                                <i class="fa fa-pencil-square-o"></i>
                                Tambah
                            </button></a>
                        </div>
                    </div>

                    <div class="table-penginapan">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th>Nama Tempat</th>
                                        <th>Wilayah</th>
                                        <th class="text-center">Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>Hotel Mulia</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="tooltip" data-placement="bottom" title="Lihat">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-inap') }}"><button type="button" class="btn btn-default btn-admin-option" data-toggle="tooltip" data-placement="bottom" title="Edit">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="modal" data-target=".delete-confirm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td>Hotel Aston</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-inap') }}"><button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td>Hotel Horizon</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-inap') }}"><button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4</td>
                                        <td>Hotel Hyatt</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-inap') }}"><button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="admin-oleholeh">
                    <div class="admin-title">
                        <div class="col-md-6">
                            <h2 class="admin-meta">Tempat Oleh-oleh</h2>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('form-oleh') }}"><button type="button" class="btn btn-default btn-admin pull-right">
                                <i class="fa fa-pencil-square-o"></i>
                                Tambah
                            </button></a>
                        </div>
                    </div>
                
                    <div class="table-oleholeh">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th>Nama Tempat</th>
                                        <th>Wilayah</th>
                                        <th class="text-center">Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>Toko Batik</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="tooltip" data-placement="bottom" title="Lihat">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-inap') }}"><button type="button" class="btn btn-default btn-admin-option" data-toggle="tooltip" data-placement="bottom" title="Edit">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="modal" data-target=".delete-confirm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td>Toko Bakpia</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-inap') }}"><button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="modal" data-target=".delete-confirm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td>Toko Wayang</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-inap') }}"><button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="modal" data-target=".delete-confirm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="admin-transit">
                    <div class="admin-title">
                        <div class="col-md-6">
                            <h2 class="admin-meta">Tempat Transit</h2>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('form-transit') }}"><button type="button" class="btn btn-default btn-admin pull-right">
                                <i class="fa fa-pencil-square-o"></i>
                                Tambah
                            </button></a>
                            <a href="{{ url('dummy-transit') }}"><button type="button" class="btn btn-default btn-admin pull-right">
                                <i class="fa fa-pencil-square-o"></i>
                                All
                            </button></a>
                        </div>
                    </div>
                
                    <div class="table-transit">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th>Nama Tempat</th>
                                        <th>Wilayah</th>
                                        <th class="text-center">Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>Stasiun Pasar Senen</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="tooltip" data-placement="bottom" title="Lihat">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-transit') }}"><button type="button" class="btn btn-default btn-admin-option" data-toggle="tooltip" data-placement="bottom" title="Edit">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="modal" data-target=".delete-confirm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td>Bandara Juanda</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-transit') }}"><button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="modal" data-target=".delete-confirm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td>Terminal Kampung Rambutan</td>
                                        <td>Bantul(?)</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url('form-transit') }}"><button type="button" class="btn btn-default btn-admin-option">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button></a>
                                            <button type="button" class="btn btn-default btn-admin-option" data-toggle="modal" data-target=".delete-confirm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="modal fade delete-confirm" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content modal-custom">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Hapus Tempat Wisata</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Apakah kamu yakin untuk menghapus ini?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-danger" id="confirm">Hapus</button>  
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
    </section>
    <script src="{{asset('/js/jquery-1.11.3.js')}}"></script>
    <script src="{{asset('/js/bootstrap.js')}}"></script>
</body>
</html>