<div class="tab-pane active" id="tab_wisata">
    <ul class="dest">
        @foreach($wisata as $item)
            <li class="dest-list col-md-12">
                <div class="col-md-5">
                    <div class="tab-img">
                        <img src="{{asset($item->gallery()->first()->foto)}}">
                    </div>
                </div>
                <div class="col-md-7">
                    <a href="#" class="dest-title" data-toggle="modal" data-target=".detail-wisata">{{$item->nama}}</a>
                    <div class="dest-star">
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                    </div>
                    <a data-lat="{{$item->lat}}" data-lng="{{$item->lng}}" class="btn btn-xs btn-default btn-view-on-map">Lihat dalam peta</a>
                </div>
            </li>
        @endforeach
    </ul>
</div>
<div class="tab-pane" id="tab_penginapan">
    <ul class="dest">
        @if(!is_null($penginapan))
            @foreach($penginapan as $item)
                <li class="dest-list col-md-12">
                    <div class="col-md-5">
                        <div class="tab-img">
                            <img src="{{asset($item->gallery()->first()->foto)}}">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <a href="#" class="dest-title">Pantai Parangtritis</a>
                        <div class="dest-star">
                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                        </div>
                    </div>
                </li>
            @endforeach
        @endif
    </ul>
</div>
<div class="tab-pane" id="tab_oleh">
    <ul class="dest">
        @if(!is_null($oleh))
            @foreach($oleh as $item)
                <li class="dest-list col-md-12">
                    <div class="col-md-5">
                        <div class="tab-img">
                            <img src="{{asset($item->gallery()->first()->foto)}}">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <a href="#" class="dest-title">Pantai Parangtritis</a>
                        <div class="dest-star">
                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                        </div>
                    </div>
                </li>
            @endforeach
        @endif
    </ul>
</div>
<div class="hidden" id="markers">{{$markers}}</div>