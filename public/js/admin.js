var map;
var latt;
var lngg;
var marker;


function initialize() {

	var mapOptions = {
        zoom: 10,
        center:new google.maps.LatLng(-7.8730445,110.4242874),
    };

    map = new google.maps.Map(document.getElementById('map-canvas'),  mapOptions);    
    marker = new google.maps.Marker({
		  position: new google.maps.LatLng(-7.8730445,110.4242874),
		  draggable:true,
		  map: map
		});
    marker.setMap(null);
    google.maps.event.addListener(map, 'click', function(event) {
	    latt=parseFloat(event.latLng.lat());
	   	lngg=parseFloat(event.latLng.lng());
	    document.getElementById('lat').value = latt;
	    document.getElementById('lng').value = lngg;

	    marker.setPosition(new google.maps.LatLng(latt,lngg))

	    marker.setMap(map);
	    
	});
	  
	google.maps.event.addListener(marker, "dragend", function(event) {
		updateMarkerPosition(marker.getPosition())
	});

}

function updateMarkerPosition(latLng) {
	document.getElementById('lat').value = parseFloat(latLng.lat());
	document.getElementById('lng').value = parseFloat(latLng.lng());
}

google.maps.event.addDomListener(window, 'load', initialize);