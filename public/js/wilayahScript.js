var map;
var varLL = [];

function initialize() {

	var mapOptions = {
        zoom: 10,
        center:new google.maps.LatLng(-7.8730445,110.4242874),
    };

    map = new google.maps.Map(document.getElementById('map_canvas'),  mapOptions);
    createMarker(-7.8730445,110.4242874,"Yogyakarta");

}

function createMarker(lat, lng, title){
	var marker=new google.maps.Marker({
	  position:new google.maps.LatLng(lat, lng),
	  map: map,
	  title : title
	});
}

function createImageMarker(lat, lng, title, image){
	var marker=new google.maps.Marker({
	  position:new google.maps.LatLng(lat, lng),
	  map: map,
	  title : title,
	  icon : image
	});
}

google.maps.event.addDomListener(window, 'load', initialize);