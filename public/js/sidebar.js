$(function() {
    var Site = {
        init: function() {
            Site.sideContent();
        },

        sideContent: function() {
          var
            $leftToggle = $('.toggle-btn-left'),
            $leftSidebar = $('.left-sidebar'),
            $leftTrigger = $leftSidebar.prev(),
            $rightToggle = $('.toggle-btn-right'),
            $rightSidebar = $('.right-sidebar'),
            $rightTrigger = $rightSidebar.prev();

            $leftTrigger.click(function() {
                if($(this).next().hasClass('active')){
                    $(this).next().removeClass('active');
                    $(this).removeClass('active');
                    $(this).css('left',0);
                    
                } else {
                    $leftSidebar.removeClass('active');
                    $(this).removeClass('active');
                    $(this).css('left',$('.left-sidebar').width());

                    $(this).next().addClass('active');
                    $(this).addClass('active');
                }
            });

            $rightTrigger.click(function() {
                if($(this).next().hasClass('active')){
                    $(this).next().removeClass('active');
                    $(this).removeClass('active');
                    $(this).css('right',0);

                } else {
                    $rightSidebar.removeClass('active');
                    $(this).removeClass('active');
                    $(this).css('right',$('.right-sidebar').width());

                    $(this).next().addClass('active');
                    $(this).addClass('active');
                }
            });

        }
    };

    Site.init();
});
    

    