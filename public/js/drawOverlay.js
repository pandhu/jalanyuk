var map;
var varLL = [];
var text = "";
var text2 = "";
var text3 = "";
var text4 = "";
var latt;
var lngg;
var pointing = true;
var state = 0;
var trip = [];
var posMarker = [];
var posDir = [];
var markers = [];
var waypointsss = [];
var flightPath;
var directionsService;
var directionsDisplay;
var request;
var dur ='6';

function initialize() {
	
	directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer();

	var mapOptions = {
        zoom: 10,
        center:new google.maps.LatLng(-7.8730445,110.4242874),
    };

    map = new google.maps.Map(document.getElementById('map_canvas'),  mapOptions);
    
    if (pointing) {
    	google.maps.event.addListener(map, 'click', function(event) {
	    	latt=parseFloat(event.latLng.lat());
	    	lngg=parseFloat(event.latLng.lng());
	    	if (state == 1) {
		    	trip.push(new google.maps.LatLng(latt,lngg ));
			    writeKoor(trip);
		    } else if (state == 2){
		    	posMarker.push(new google.maps.LatLng(latt,lngg));
		    	writeMarker(posMarker);
		    } else if(state == 3){
		    	posDir.push(new google.maps.LatLng(latt,lngg));
		    	writeDir(posDir);
		    } else {

		    };
	    });
    };
    
}

function changeState(x){
	state = x;
}

function createMarker(pos, title){
	var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
      '<div id="bodyContent">'+
      '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
      'sandstone rock formation in the southern part of the '+
      'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
      'south west of the nearest large town, Alice Springs; 450&#160;km '+
      '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
      'features of the Uluru - Kata Tjuta National Park. Uluru is '+
      'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
      'Aboriginal people of the area. It has many springs, waterholes, '+
      'rock caves and ancient paintings. Uluru is listed as a World '+
      'Heritage Site.</p>'+
      '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
      'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
      '(last visited June 22, 2009).</p>'+
      '</div>'+
      '</div>';

	var infowindow = new google.maps.InfoWindow({
	    content: contentString
	  });

	var marker=new google.maps.Marker({
	  position:pos,
	  map: map,
	  title : title
	});
	marker.addListener('click', function() {
	    infowindow.open(map, marker);
	  });

	markers.push(marker);
}

function writeMarker(markerr){
	text2 ="";
	for (i = 0; i < markerr.length; i++) { 
    	text2 +=
    	'<div>'
    		+markerr[i] + '<br>'
    		+'<button type="button" class="btn btn-primary" value='+i+' onclick="drawMarker(this.value)">Draw Marker</button>'
    		+'<button type="button" class="btn btn-danger" value='+i+' onclick="removeMarker(this.value)">Remove</button>' 
    	+'</div>';
	}
	document.getElementById("marker").innerHTML = text2;
}

function removeMarker (x) {
	posMarker.splice(x, 1);
	writeMarker(posMarker);
	markers[x].setMap(null);
	markers.splice(x,1);
}

function drawMarker(x){
	createMarker(posMarker[x], "random");
}

function createImageMarker(lat, lng, title, image){
	var marker=new google.maps.Marker({
	  position:new google.maps.LatLng(lat, lng),
	  map: map,
	  title : title,
	  icon : image
	});
}

function drawPolygon(myTrip){
	
	removeArea();

	flightPath=new google.maps.Polygon({
	  path:myTrip,
	  strokeColor:"#0000FF",
	  strokeOpacity:0.8,
	  strokeWeight:2,
	  fillColor:"#0000FF",
	  fillOpacity:0.4
	  });

	flightPath.setMap(map);
}

function removeArea () {
	if (flightPath != null)
		flightPath.setMap(null);
}

function drawArea(){
	drawPolygon(trip);
}

function writeKoor(koornya){
	text ="";
	for (i = 0; i < koornya.length; i++) { 
    	text +=
    	'<div>'
    		+koornya[i] + '<br>'
    		+'<button type="button" class="btn btn-danger" value='+i+' onclick="removeKoor(this.value)">Remove</button>' 
    	+'</div>';
	}
	document.getElementById("koordinat").innerHTML = text;
}

function removeKoor(x){
	trip.splice(x, 1);
	writeKoor(trip);
}

function writeDir(dir){
	text3 ="";
	for (i = 0; i < dir.length; i++) { 
    	text3 +=
    	'<div>'
    		+dir[i] + '<br>'
    		+'<button type="button" class="btn btn-danger" value='+i+' onclick="removeDir(this.value)">Remove</button>' 
    	+'</div>';
	}
	document.getElementById("route").innerHTML = text3;
}

function removeDir(x){
	posDir.splice(x, 1);
	writeDir(posDir);
}

function makeDirection(){
	directionsDisplay.setMap(null);
	waypointsss = [];
	for (var i = 1; i < posDir.length -1; i++) {
	  waypointsss.push({
	     location: posDir[i],
	     stopover:true
	  }); 
	};

	request = {
		origin: posDir[0], 
       	destination: posDir[posDir.length -1],
       	waypoints: waypointsss,
		provideRouteAlternatives: true,
       	travelMode: google.maps.DirectionsTravelMode.DRIVING
	};
	directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            dur = response.routes[0].legs[0].duration.value;
            document.getElementById('directions-panel').innerHTML = dur;
        }
    });
    directionsDisplay.setMap(map);
    
}

google.maps.event.addDomListener(window, 'load', initialize);