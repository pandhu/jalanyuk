var map, placesList;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var ploc = null;


function initialize() {
    /*/
     var ploc = new google.maps.LatLng(-6.344303, 106.847448);
     map = new google.maps.Map(document.getElementById('map-canvas'), {
     center: ploc,
     zoom: 10
     });
     //*/

    directionsDisplay = new google.maps.DirectionsRenderer();

    var mapOptions = {
        zoom: 16,
    };

    var pmarker;

    map = new google.maps.Map(document.getElementById('map_canvas'),  mapOptions);
    directionsDisplay.setMap(map);
    //map.setCenter({lat:-7.771447,lng 110.377423});

//google map custom marker icon - .png fallback for IE11
    var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
    var marker_url = ( is_internetExplorer11 ) ? 'img/cd-icon-location.png' : 'img/cd-icon-location.svg';



    //separator===================== geolocation
    //
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            ploc = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var ploc2 = new google.maps.LatLng(position.coords.latitude+0.002, position.coords.longitude);
            //create info box

            var infowindow = new google.maps.InfoWindow({
                content: 'You are here !'
            });

            //create marker
            pmarker = new google.maps.Marker({
                map:map,
                draggable:true,
                //animation: google.maps.Animation.BOUNCE,
                position: ploc,
                icon: marker_url
            });

            infowindow.open(pmarker.get('map'), pmarker);


            if(ploc!=null){
                //===========================search masjid call
                var request = {
                    location: ploc,
                    radius: 1500,
                    types: ['mosque']
                };

                placesList = document.getElementById('places');

                var service = new google.maps.places.PlacesService(map);
                service.nearbySearch(request, callback);
                //===========================end

            }

            map.setCenter(ploc);
            map.setZoom(15);
        }, function() {
            handleNoGeolocation(true);
        });
    } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
    }
    //*/

    //we define here the style of the map


}
//********End of Func***********//

//geolocation fail handling function
function handleNoGeolocation(errorFlag) {
    if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
    } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
    }

    var options = {
        map: map,
        position: new google.maps.LatLng(60, 105),
        content: content,

    };

    var infowindow = new google.maps.InfoWindow(options);
    map.setCenter(options.position);
}

//nearest masjid function
function callback(results, status, pagination) {
    if (status != google.maps.places.PlacesServiceStatus.OK) {
        return;
    } else {
        createMarkers(results);

        if (pagination.hasNextPage) {
            var moreButton = document.getElementById('more');

            moreButton.disabled = false;

            google.maps.event.addDomListenerOnce(moreButton, 'click',
                function() {
                    moreButton.disabled = true;
                    pagination.nextPage();
                });
        }
    }
}

function createMarkers(places) {
    var bounds = new google.maps.LatLngBounds();

    for (var i = 0, place; place = places[i]; i++) {
        var image = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
        };

        var marker = new google.maps.Marker({
            map: map,
            icon: image,
            title: place.name,
            position: place.geometry.location
        });

        placesList.innerHTML += '<li>' + place.name + '</li>';
        bounds.extend(place.geometry.location);
        marker.setTitle((i + 1).toString());

        addDirectionListener(marker);
        /*/add listener to get directions
         google.maps.event.addListener(marker, 'click', function() {

         alert(i);
         //*  get direction function buggy
         var request = {
         origin: ploc,
         destination: marker.position,
         travelMode: google.maps.TravelMode.WALKING
         };
         //alert(marker.position.toString());
         directionsService.route(request, function(response, status) {
         if (status == google.maps.DirectionsStatus.OK) {
         directionsDisplay.setDirections(response);
         //alert("jalan gan");
         }
         });

         });
         //*/

    }
    //map.fitBounds(bounds);
}

//add listener to get directions function buggy
function addDirectionListener(marker){
    google.maps.event.addListener(marker, 'click', function() {

        //alert(i);
        //*  get direction function buggy
        var request = {
            origin: ploc,
            destination: marker.position,
            travelMode: google.maps.TravelMode.WALKING
        };
        //alert(marker.position.toString());
        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                //alert("jalan gan");
            }
        });
    });
}

//show hide function
function showhide() {
    var hidebtn = document.getElementById("hidebtn");
    var divresults = document.getElementById("results");

    if (divresults.style.display !== "none") {
        divresults.style.display = "none";
        hidebtn.innerHTML="Show";
    }
    else {
        divresults.style.display = "block";
        hidebtn.innerHTML="Hide";
    }
}
google.maps.event.addDomListener(window, 'load', initialize);