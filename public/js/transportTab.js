$(document).on('click', 'div.bhoechie-tab-menu>div.list-group>a', function(e) {
    e.preventDefault();
    
    $content = $('div.bhoechie-tab>div.bhoechie-tab-content');
    
    $(this).siblings('a.active').removeClass("active");
    $(this).addClass("active");
    
    var index = $(this).index();
    
    $content.removeClass("active");
    $content.eq(index).addClass("active");
});